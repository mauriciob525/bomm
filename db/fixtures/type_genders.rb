TypeGender.seed do |s|
  s.id    = 1
  s.type_gender_name  = "Balada "
end
TypeGender.seed do |s|
  s.id    = 2
  s.type_gender_name  = "Blues"
end
TypeGender.seed do |s|
  s.id    = 3
  s.type_gender_name  = "Bolero"
end
TypeGender.seed do |s|
  s.id    = 4
  s.type_gender_name  = "Cumbia"
end
TypeGender.seed do |s|
  s.id    = 5
  s.type_gender_name  = "Electrónica"
end
TypeGender.seed do |s|
  s.id    = 6
  s.type_gender_name  = "Experimental"
end
TypeGender.seed do |s|
  s.id    = 7
  s.type_gender_name  = "Folclorico"
end
TypeGender.seed do |s|
  s.id    = 8
  s.type_gender_name  = "Fusión"
end
TypeGender.seed do |s|
  s.id    = 9
  s.type_gender_name  = "Heavy Metal"
end
TypeGender.seed do |s|
  s.id    = 10
  s.type_gender_name  = "Hip-Hop/Rap"
end
TypeGender.seed do |s|
  s.id    = 11
  s.type_gender_name  = "Indie"
end
TypeGender.seed do |s|
  s.id    = 12
  s.type_gender_name  = "Jazz"
end
TypeGender.seed do |s|
  s.id    = 13
  s.type_gender_name  = "Merengue"
end
TypeGender.seed do |s|
  s.id    = 14
  s.type_gender_name  = "Música Andina"
end
TypeGender.seed do |s|
  s.id    = 15
  s.type_gender_name  = "Música Clásica/Académica/Opera"
end
TypeGender.seed do |s|
  s.id    = 16
  s.type_gender_name  = "Música Infantil"
end
TypeGender.seed do |s|
  s.id    = 17
  s.type_gender_name  = "Música llanera"
end
TypeGender.seed do |s|
  s.id    = 18
  s.type_gender_name  = "Pop Alternativo"
end
TypeGender.seed do |s|
  s.id    = 19
  s.type_gender_name  = "Pop Electrónico"
end
TypeGender.seed do |s|
  s.id    = 20
  s.type_gender_name  = "Pop Rock"
end
TypeGender.seed do |s|
  s.id    = 21
  s.type_gender_name  = "Popular/Mariachi"
end
TypeGender.seed do |s|
  s.id    = 22
  s.type_gender_name  = "Punk/Hardcore"
end
TypeGender.seed do |s|
  s.id    = 23
  s.type_gender_name  = "R&B/Soul"
end
TypeGender.seed do |s|
  s.id    = 24
  s.type_gender_name  = "Reggae"
end
TypeGender.seed do |s|
  s.id    = 25
  s.type_gender_name  = "Reggaeton/Urbano"
end
TypeGender.seed do |s|
  s.id    = 26
  s.type_gender_name  = "Rock"
end
TypeGender.seed do |s|
  s.id    = 27
  s.type_gender_name  = "Salsa y Son"
end
TypeGender.seed do |s|
  s.id    = 28
  s.type_gender_name  = "Ska"
end
TypeGender.seed do |s|
  s.id    = 29
  s.type_gender_name  = "Tango"
end
TypeGender.seed do |s|
  s.id    = 30
  s.type_gender_name  = "Vallenato"
end
TypeGender.seed do |s|
  s.id    = 31
  s.type_gender_name  = "Vocal y Acapella"
end
TypeGender.seed do |s|
  s.id    = 32
  s.type_gender_name  = "World Music"
end