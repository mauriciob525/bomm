#DEPARTMENT
Department.seed do |s|
  s.id    = 1
  s.department_name  =  'Amazonas'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 2
  s.department_name  =  'Antioquia'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 3
  s.department_name  =  'Arauca'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 4
  s.department_name  =  'Atlantico'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 5
  s.department_name  =  'Bogotá'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 6
  s.department_name  =  'Bolivar'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 7
  s.department_name  =  'Boyaca'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 8
  s.department_name  =  'Caldas'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 9
  s.department_name  =  'Caqueta'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 10
  s.department_name  =  'Casanare'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 11
  s.department_name  =  'Cauca'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 12
  s.department_name  =  'Cesar'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 13
  s.department_name  =  'Choco'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 14
  s.department_name  =  'Cordoba'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 15
  s.department_name  =  'Cundinamarca'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 16
  s.department_name  =  'Guainia'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 17
  s.department_name  =  'Guaviare'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 18
  s.department_name  =  'Huila'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 19
  s.department_name  =  'La Guajira'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 20
  s.department_name  =  'Magdalena'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 21
  s.department_name  =  'Meta'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 22
  s.department_name  =  'Nariño'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 23
  s.department_name  =  'Norte de Santander'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 24
  s.department_name  =  'Putumayo'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 25
  s.department_name  =  'Quindio'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 26
  s.department_name  =  'Risaralda'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 27
  s.department_name  =  'San Andres'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 28
  s.department_name  =  'Santander'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 29
  s.department_name  =  'Sucre'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 30
  s.department_name  =  'Tolima'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 31
  s.department_name  =  'Valle'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 32
  s.department_name  =  'Vaupes'
  s.country_id  = 1
end
Department.seed do |s|
  s.id    = 33
  s.department_name  =  'Vichada'
  s.country_id  = 1
end