City.seed do |s|
  s.id    = 1
  s.city_name  = "LETICIA"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 2
  s.city_name  = "EL ENCANTO"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 3
  s.city_name  = "LA CHORRERA"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 4
  s.city_name  = "LA PEDRERA"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 5
  s.city_name  = "LA VICTORIA"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 6
  s.city_name  = "MIRITI - PARANÁ"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 7
  s.city_name  = "PUERTO ALEGRÍA"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 8
  s.city_name  = "PUERTO ARICA"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 9
  s.city_name  = "PUERTO NARIÑO"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 10
  s.city_name  = "PUERTO SANTANDER"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 11
  s.city_name  = "TARAPACÁ"
  s.department_id  = 1
end
City.seed do |s|
  s.id    = 12
  s.city_name  = "MEDELLÍN"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 13
  s.city_name  = "ABEJORRAL"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 14
  s.city_name  = "ABRIAQUÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 15
  s.city_name  = "ALEJANDRÍA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 16
  s.city_name  = "AMAGÁ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 17
  s.city_name  = "AMALFI"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 18
  s.city_name  = "ANDES"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 19
  s.city_name  = "ANGELÓPOLIS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 20
  s.city_name  = "ANGOSTURA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 21
  s.city_name  = "ANORÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 22
  s.city_name  = "SANTAFÉ DE ANTIOQUIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 23
  s.city_name  = "ANZA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 24
  s.city_name  = "APARTADÓ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 25
  s.city_name  = "ARBOLETES"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 26
  s.city_name  = "ARGELIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 27
  s.city_name  = "ARMENIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 28
  s.city_name  = "BARBOSA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 29
  s.city_name  = "BELMIRA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 30
  s.city_name  = "BELLO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 31
  s.city_name  = "BETANIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 32
  s.city_name  = "BETULIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 33
  s.city_name  = "CIUDAD BOLÍVAR"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 34
  s.city_name  = "BRICEÑO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 35
  s.city_name  = "BURITICÁ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 36
  s.city_name  = "CÁCERES"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 37
  s.city_name  = "CAICEDO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 38
  s.city_name  = "CALDAS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 39
  s.city_name  = "CAMPAMENTO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 40
  s.city_name  = "CAÑASGORDAS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 41
  s.city_name  = "CARACOLÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 42
  s.city_name  = "CARAMANTA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 43
  s.city_name  = "CAREPA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 44
  s.city_name  = "EL CARMEN DE VIBORAL"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 45
  s.city_name  = "CAROLINA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 46
  s.city_name  = "CAUCASIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 47
  s.city_name  = "CHIGORODÓ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 48
  s.city_name  = "CISNEROS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 49
  s.city_name  = "COCORNÁ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 50
  s.city_name  = "CONCEPCIÓN"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 51
  s.city_name  = "CONCORDIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 52
  s.city_name  = "COPACABANA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 53
  s.city_name  = "DABEIBA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 54
  s.city_name  = "DONMATÍAS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 55
  s.city_name  = "EBÉJICO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 56
  s.city_name  = "EL BAGRE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 57
  s.city_name  = "ENTRERRIOS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 58
  s.city_name  = "ENVIGADO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 59
  s.city_name  = "FREDONIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 60
  s.city_name  = "FRONTINO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 61
  s.city_name  = "GIRALDO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 62
  s.city_name  = "GIRARDOTA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 63
  s.city_name  = "GÓMEZ PLATA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 64
  s.city_name  = "GRANADA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 65
  s.city_name  = "GUADALUPE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 66
  s.city_name  = "GUARNE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 67
  s.city_name  = "GUATAPE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 68
  s.city_name  = "HELICONIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 69
  s.city_name  = "HISPANIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 70
  s.city_name  = "ITAGUI"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 71
  s.city_name  = "ITUANGO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 72
  s.city_name  = "JARDÍN"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 73
  s.city_name  = "JERICÓ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 74
  s.city_name  = "LA CEJA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 75
  s.city_name  = "LA ESTRELLA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 76
  s.city_name  = "LA PINTADA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 77
  s.city_name  = "LA UNIÓN"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 78
  s.city_name  = "LIBORINA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 79
  s.city_name  = "MACEO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 80
  s.city_name  = "MARINILLA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 81
  s.city_name  = "MONTEBELLO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 82
  s.city_name  = "MURINDÓ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 83
  s.city_name  = "MUTATÁ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 84
  s.city_name  = "NARIÑO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 85
  s.city_name  = "NECOCLÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 86
  s.city_name  = "NECHÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 87
  s.city_name  = "OLAYA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 88
  s.city_name  = "PEÑOL"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 89
  s.city_name  = "PEQUE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 90
  s.city_name  = "PUEBLORRICO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 91
  s.city_name  = "PUERTO BERRÍO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 92
  s.city_name  = "PUERTO NARE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 93
  s.city_name  = "PUERTO TRIUNFO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 94
  s.city_name  = "REMEDIOS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 95
  s.city_name  = "RETIRO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 96
  s.city_name  = "RIONEGRO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 97
  s.city_name  = "SABANALARGA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 98
  s.city_name  = "SABANETA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 99
  s.city_name  = "SALGAR"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 100
  s.city_name  = "SAN ANDRÉS DE CUERQUÍA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 101
  s.city_name  = "SAN CARLOS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 102
  s.city_name  = "SAN FRANCISCO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 103
  s.city_name  = "SAN JERÓNIMO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 104
  s.city_name  = "SAN JOSÉ DE LA MONTAÑA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 105
  s.city_name  = "SAN JUAN DE URABÁ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 106
  s.city_name  = "SAN LUIS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 107
  s.city_name  = "SAN PEDRO DE LOS MILAGROS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 108
  s.city_name  = "SAN PEDRO DE URABA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 109
  s.city_name  = "SAN RAFAEL"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 110
  s.city_name  = "SAN ROQUE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 111
  s.city_name  = "SAN VICENTE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 112
  s.city_name  = "SANTA BÁRBARA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 113
  s.city_name  = "SANTA ROSA DE OSOS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 114
  s.city_name  = "SANTO DOMINGO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 115
  s.city_name  = "EL SANTUARIO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 116
  s.city_name  = "SEGOVIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 117
  s.city_name  = "SONSON"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 118
  s.city_name  = "SOPETRÁN"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 119
  s.city_name  = "TÁMESIS"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 120
  s.city_name  = "TARAZÁ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 121
  s.city_name  = "TARSO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 122
  s.city_name  = "TITIRIBÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 123
  s.city_name  = "TOLEDO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 124
  s.city_name  = "TURBO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 125
  s.city_name  = "URAMITA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 126
  s.city_name  = "URRAO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 127
  s.city_name  = "VALDIVIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 128
  s.city_name  = "VALPARAÍSO"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 129
  s.city_name  = "VEGACHÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 130
  s.city_name  = "VENECIA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 131
  s.city_name  = "VIGÍA DEL FUERTE"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 132
  s.city_name  = "YALÍ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 133
  s.city_name  = "YARUMAL"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 134
  s.city_name  = "YOLOMBÓ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 135
  s.city_name  = "YONDÓ"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 136
  s.city_name  = "ZARAGOZA"
  s.department_id  = 2
end
City.seed do |s|
  s.id    = 137
  s.city_name  = "ARAUCA"
  s.department_id  = 3
end
City.seed do |s|
  s.id    = 138
  s.city_name  = "ARAUQUITA"
  s.department_id  = 3
end
City.seed do |s|
  s.id    = 139
  s.city_name  = "CRAVO NORTE"
  s.department_id  = 3
end
City.seed do |s|
  s.id    = 140
  s.city_name  = "FORTUL"
  s.department_id  = 3
end
City.seed do |s|
  s.id    = 141
  s.city_name  = "PUERTO RONDÓN"
  s.department_id  = 3
end
City.seed do |s|
  s.id    = 142
  s.city_name  = "SARAVENA"
  s.department_id  = 3
end
City.seed do |s|
  s.id    = 143
  s.city_name  = "TAME"
  s.department_id  = 3
end
City.seed do |s|
  s.id    = 144
  s.city_name  = "BARRANQUILLA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 145
  s.city_name  = "BARANOA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 146
  s.city_name  = "CAMPO DE LA CRUZ"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 147
  s.city_name  = "CANDELARIA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 148
  s.city_name  = "GALAPA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 149
  s.city_name  = "JUAN DE ACOSTA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 150
  s.city_name  = "LURUACO"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 151
  s.city_name  = "MALAMBO"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 152
  s.city_name  = "MANATÍ"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 153
  s.city_name  = "PALMAR DE VARELA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 154
  s.city_name  = "PIOJÓ"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 155
  s.city_name  = "POLONUEVO"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 156
  s.city_name  = "PONEDERA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 157
  s.city_name  = "PUERTO COLOMBIA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 158
  s.city_name  = "REPELÓN"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 159
  s.city_name  = "SABANAGRANDE"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 160
  s.city_name  = "SABANALARGA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 161
  s.city_name  = "SANTA LUCÍA"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 162
  s.city_name  = "SANTO TOMÁS"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 163
  s.city_name  = "SOLEDAD"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 164
  s.city_name  = "SUAN"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 165
  s.city_name  = "TUBARÁ"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 166
  s.city_name  = "USIACURÍ"
  s.department_id  = 4
end
City.seed do |s|
  s.id    = 167
  s.city_name  = "BOGOTÁ"
  s.department_id  = 5
end
City.seed do |s|
  s.id    = 168
  s.city_name  = "CARTAGENA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 169
  s.city_name  = "ACHÍ"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 170
  s.city_name  = "ALTOS DEL ROSARIO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 171
  s.city_name  = "ARENAL"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 172
  s.city_name  = "ARJONA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 173
  s.city_name  = "ARROYOHONDO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 174
  s.city_name  = "BARRANCO DE LOBA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 175
  s.city_name  = "CALAMAR"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 176
  s.city_name  = "CANTAGALLO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 177
  s.city_name  = "CICUCO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 178
  s.city_name  = "CÓRDOBA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 179
  s.city_name  = "CLEMENCIA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 180
  s.city_name  = "EL CARMEN DE BOLÍVAR"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 181
  s.city_name  = "EL GUAMO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 182
  s.city_name  = "EL PEÑÓN"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 183
  s.city_name  = "HATILLO DE LOBA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 184
  s.city_name  = "MAGANGUÉ"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 185
  s.city_name  = "MAHATES"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 186
  s.city_name  = "MARGARITA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 187
  s.city_name  = "MARÍA LA BAJA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 188
  s.city_name  = "MONTECRISTO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 189
  s.city_name  = "MOMPÓS"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 190
  s.city_name  = "MORALES"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 191
  s.city_name  = "NOROSÍ"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 192
  s.city_name  = "PINILLOS"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 193
  s.city_name  = "REGIDOR"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 194
  s.city_name  = "RÍO VIEJO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 195
  s.city_name  = "SAN CRISTÓBAL"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 196
  s.city_name  = "SAN ESTANISLAO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 197
  s.city_name  = "SAN FERNANDO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 198
  s.city_name  = "SAN JACINTO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 199
  s.city_name  = "SAN JACINTO DEL CAUCA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 200
  s.city_name  = "SAN JUAN NEPOMUCENO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 201
  s.city_name  = "SAN MARTÍN DE LOBA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 202
  s.city_name  = "SAN PABLO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 203
  s.city_name  = "SANTA CATALINA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 204
  s.city_name  = "SANTA ROSA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 205
  s.city_name  = "SANTA ROSA DEL SUR"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 206
  s.city_name  = "SIMITÍ"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 207
  s.city_name  = "SOPLAVIENTO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 208
  s.city_name  = "TALAIGUA NUEVO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 209
  s.city_name  = "TIQUISIO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 210
  s.city_name  = "TURBACO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 211
  s.city_name  = "TURBANÁ"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 212
  s.city_name  = "VILLANUEVA"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 213
  s.city_name  = "ZAMBRANO"
  s.department_id  = 6
end
City.seed do |s|
  s.id    = 214
  s.city_name  = "TUNJA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 215
  s.city_name  = "ALMEIDA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 216
  s.city_name  = "AQUITANIA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 217
  s.city_name  = "ARCABUCO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 218
  s.city_name  = "BELÉN"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 219
  s.city_name  = "BERBEO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 220
  s.city_name  = "BETÉITIVA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 221
  s.city_name  = "BOAVITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 222
  s.city_name  = "BOYACÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 223
  s.city_name  = "BRICEÑO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 224
  s.city_name  = "BUENAVISTA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 225
  s.city_name  = "BUSBANZÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 226
  s.city_name  = "CALDAS"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 227
  s.city_name  = "CAMPOHERMOSO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 228
  s.city_name  = "CERINZA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 229
  s.city_name  = "CHINAVITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 230
  s.city_name  = "CHIQUINQUIRÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 231
  s.city_name  = "CHISCAS"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 232
  s.city_name  = "CHITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 233
  s.city_name  = "CHITARAQUE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 234
  s.city_name  = "CHIVATÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 235
  s.city_name  = "CIÉNEGA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 236
  s.city_name  = "CÓMBITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 237
  s.city_name  = "COPER"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 238
  s.city_name  = "CORRALES"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 239
  s.city_name  = "COVARACHÍA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 240
  s.city_name  = "CUBARÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 241
  s.city_name  = "CUCAITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 242
  s.city_name  = "CUÍTIVA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 243
  s.city_name  = "CHÍQUIZA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 244
  s.city_name  = "CHIVOR"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 245
  s.city_name  = "DUITAMA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 246
  s.city_name  = "EL COCUY"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 247
  s.city_name  = "EL ESPINO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 248
  s.city_name  = "FIRAVITOBA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 249
  s.city_name  = "FLORESTA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 250
  s.city_name  = "GACHANTIVÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 251
  s.city_name  = "GAMEZA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 252
  s.city_name  = "GARAGOA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 253
  s.city_name  = "GUACAMAYAS"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 254
  s.city_name  = "GUATEQUE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 255
  s.city_name  = "GUAYATÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 256
  s.city_name  = "GÜICÁN"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 257
  s.city_name  = "IZA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 258
  s.city_name  = "JENESANO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 259
  s.city_name  = "JERICÓ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 260
  s.city_name  = "LABRANZAGRANDE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 261
  s.city_name  = "LA CAPILLA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 262
  s.city_name  = "LA VICTORIA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 263
  s.city_name  = "LA UVITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 264
  s.city_name  = "VILLA DE LEYVA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 265
  s.city_name  = "MACANAL"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 266
  s.city_name  = "MARIPÍ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 267
  s.city_name  = "MIRAFLORES"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 268
  s.city_name  = "MONGUA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 269
  s.city_name  = "MONGUÍ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 270
  s.city_name  = "MONIQUIRÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 271
  s.city_name  = "MOTAVITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 272
  s.city_name  = "MUZO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 273
  s.city_name  = "NOBSA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 274
  s.city_name  = "NUEVO COLÓN"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 275
  s.city_name  = "OICATÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 276
  s.city_name  = "OTANCHE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 277
  s.city_name  = "PACHAVITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 278
  s.city_name  = "PÁEZ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 279
  s.city_name  = "PAIPA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 280
  s.city_name  = "PAJARITO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 281
  s.city_name  = "PANQUEBA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 282
  s.city_name  = "PAUNA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 283
  s.city_name  = "PAYA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 284
  s.city_name  = "PAZ DE RÍO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 285
  s.city_name  = "PESCA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 286
  s.city_name  = "PISBA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 287
  s.city_name  = "PUERTO BOYACÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 288
  s.city_name  = "QUÍPAMA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 289
  s.city_name  = "RAMIRIQUÍ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 290
  s.city_name  = "RÁQUIRA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 291
  s.city_name  = "RONDÓN"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 292
  s.city_name  = "SABOYÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 293
  s.city_name  = "SÁCHICA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 294
  s.city_name  = "SAMACÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 295
  s.city_name  = "SAN EDUARDO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 296
  s.city_name  = "SAN JOSÉ DE PARE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 297
  s.city_name  = "SAN LUIS DE GACENO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 298
  s.city_name  = "SAN MATEO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 299
  s.city_name  = "SAN MIGUEL DE SEMA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 300
  s.city_name  = "SAN PABLO DE BORBUR"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 301
  s.city_name  = "SANTANA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 302
  s.city_name  = "SANTA MARÍA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 303
  s.city_name  = "SANTA ROSA DE VITERBO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 304
  s.city_name  = "SANTA SOFÍA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 305
  s.city_name  = "SATIVANORTE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 306
  s.city_name  = "SATIVASUR"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 307
  s.city_name  = "SIACHOQUE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 308
  s.city_name  = "SOATÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 309
  s.city_name  = "SOCOTÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 310
  s.city_name  = "SOCHA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 311
  s.city_name  = "SOGAMOSO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 312
  s.city_name  = "SOMONDOCO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 313
  s.city_name  = "SORA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 314
  s.city_name  = "SOTAQUIRÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 315
  s.city_name  = "SORACÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 316
  s.city_name  = "SUSACÓN"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 317
  s.city_name  = "SUTAMARCHÁN"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 318
  s.city_name  = "SUTATENZA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 319
  s.city_name  = "TASCO"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 320
  s.city_name  = "TENZA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 321
  s.city_name  = "TIBANÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 322
  s.city_name  = "TIBASOSA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 323
  s.city_name  = "TINJACÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 324
  s.city_name  = "TIPACOQUE"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 325
  s.city_name  = "TOCA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 326
  s.city_name  = "TOGÜÍ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 327
  s.city_name  = "TÓPAGA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 328
  s.city_name  = "TOTA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 329
  s.city_name  = "TUNUNGUÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 330
  s.city_name  = "TURMEQUÉ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 331
  s.city_name  = "TUTA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 332
  s.city_name  = "TUTAZÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 333
  s.city_name  = "UMBITA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 334
  s.city_name  = "VENTAQUEMADA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 335
  s.city_name  = "VIRACACHÁ"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 336
  s.city_name  = "ZETAQUIRA"
  s.department_id  = 7
end
City.seed do |s|
  s.id    = 337
  s.city_name  = "MANIZALES"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 338
  s.city_name  = "AGUADAS"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 339
  s.city_name  = "ANSERMA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 340
  s.city_name  = "ARANZAZU"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 341
  s.city_name  = "BELALCÁZAR"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 342
  s.city_name  = "CHINCHINÁ"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 343
  s.city_name  = "FILADELFIA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 344
  s.city_name  = "LA DORADA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 345
  s.city_name  = "LA MERCED"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 346
  s.city_name  = "MANZANARES"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 347
  s.city_name  = "MARMATO"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 348
  s.city_name  = "MARQUETALIA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 349
  s.city_name  = "MARULANDA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 350
  s.city_name  = "NEIRA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 351
  s.city_name  = "NORCASIA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 352
  s.city_name  = "PÁCORA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 353
  s.city_name  = "PALESTINA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 354
  s.city_name  = "PENSILVANIA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 355
  s.city_name  = "RIOSUCIO"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 356
  s.city_name  = "RISARALDA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 357
  s.city_name  = "SALAMINA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 358
  s.city_name  = "SAMANÁ"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 359
  s.city_name  = "SAN JOSÉ"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 360
  s.city_name  = "SUPÍA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 361
  s.city_name  = "VICTORIA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 362
  s.city_name  = "VILLAMARÍA"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 363
  s.city_name  = "VITERBO"
  s.department_id  = 8
end
City.seed do |s|
  s.id    = 364
  s.city_name  = "FLORENCIA"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 365
  s.city_name  = "ALBANIA"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 366
  s.city_name  = "BELÉN DE LOS ANDAQUÍES"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 367
  s.city_name  = "CARTAGENA DEL CHAIRÁ"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 368
  s.city_name  = "CURILLO"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 369
  s.city_name  = "EL DONCELLO"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 370
  s.city_name  = "EL PAUJIL"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 371
  s.city_name  = "LA MONTAÑITA"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 372
  s.city_name  = "MILÁN"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 373
  s.city_name  = "MORELIA"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 374
  s.city_name  = "PUERTO RICO"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 375
  s.city_name  = "SAN JOSÉ DEL FRAGUA"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 376
  s.city_name  = "SAN VICENTE DEL CAGUÁN"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 377
  s.city_name  = "SOLANO"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 378
  s.city_name  = "SOLITA"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 379
  s.city_name  = "VALPARAÍSO"
  s.department_id  = 9
end
City.seed do |s|
  s.id    = 380
  s.city_name  = "YOPAL"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 381
  s.city_name  = "AGUAZUL"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 382
  s.city_name  = "CHAMEZA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 383
  s.city_name  = "HATO COROZAL"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 384
  s.city_name  = "LA SALINA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 385
  s.city_name  = "MANÍ"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 386
  s.city_name  = "MONTERREY"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 387
  s.city_name  = "NUNCHÍA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 388
  s.city_name  = "OROCUÉ"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 389
  s.city_name  = "PAZ DE ARIPORO"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 390
  s.city_name  = "PORE"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 391
  s.city_name  = "RECETOR"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 392
  s.city_name  = "SABANALARGA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 393
  s.city_name  = "SÁCAMA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 394
  s.city_name  = "SAN LUIS DE PALENQUE"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 395
  s.city_name  = "TÁMARA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 396
  s.city_name  = "TAURAMENA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 397
  s.city_name  = "TRINIDAD"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 398
  s.city_name  = "VILLANUEVA"
  s.department_id  = 10
end
City.seed do |s|
  s.id    = 399
  s.city_name  = "POPAYÁN"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 400
  s.city_name  = "ALMAGUER"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 401
  s.city_name  = "ARGELIA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 402
  s.city_name  = "BALBOA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 403
  s.city_name  = "BOLÍVAR"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 404
  s.city_name  = "BUENOS AIRES"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 405
  s.city_name  = "CAJIBÍO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 406
  s.city_name  = "CALDONO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 407
  s.city_name  = "CALOTO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 408
  s.city_name  = "CORINTO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 409
  s.city_name  = "EL TAMBO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 410
  s.city_name  = "FLORENCIA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 411
  s.city_name  = "GUACHENÉ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 412
  s.city_name  = "GUAPI"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 413
  s.city_name  = "INZÁ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 414
  s.city_name  = "JAMBALÓ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 415
  s.city_name  = "LA SIERRA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 416
  s.city_name  = "LA VEGA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 417
  s.city_name  = "LÓPEZ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 418
  s.city_name  = "MERCADERES"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 419
  s.city_name  = "MIRANDA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 420
  s.city_name  = "MORALES"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 421
  s.city_name  = "PADILLA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 422
  s.city_name  = "PAEZ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 423
  s.city_name  = "PATÍA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 424
  s.city_name  = "PIAMONTE"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 425
  s.city_name  = "PIENDAMÓ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 426
  s.city_name  = "PUERTO TEJADA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 427
  s.city_name  = "PURACÉ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 428
  s.city_name  = "ROSAS"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 429
  s.city_name  = "SAN SEBASTIÁN"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 430
  s.city_name  = "SANTANDER DE QUILICHAO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 431
  s.city_name  = "SANTA ROSA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 432
  s.city_name  = "SILVIA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 433
  s.city_name  = "SOTARA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 434
  s.city_name  = "SUÁREZ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 435
  s.city_name  = "SUCRE"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 436
  s.city_name  = "TIMBÍO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 437
  s.city_name  = "TIMBIQUÍ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 438
  s.city_name  = "TORIBIO"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 439
  s.city_name  = "TOTORÓ"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 440
  s.city_name  = "VILLA RICA"
  s.department_id  = 11
end
City.seed do |s|
  s.id    = 441
  s.city_name  = "VALLEDUPAR"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 442
  s.city_name  = "AGUACHICA"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 443
  s.city_name  = "AGUSTÍN CODAZZI"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 444
  s.city_name  = "ASTREA"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 445
  s.city_name  = "BECERRIL"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 446
  s.city_name  = "BOSCONIA"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 447
  s.city_name  = "CHIMICHAGUA"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 448
  s.city_name  = "CHIRIGUANÁ"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 449
  s.city_name  = "CURUMANÍ"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 450
  s.city_name  = "EL COPEY"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 451
  s.city_name  = "EL PASO"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 452
  s.city_name  = "GAMARRA"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 453
  s.city_name  = "GONZÁLEZ"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 454
  s.city_name  = "LA GLORIA"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 455
  s.city_name  = "LA JAGUA DE IBIRICO"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 456
  s.city_name  = "MANAURE"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 457
  s.city_name  = "PAILITAS"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 458
  s.city_name  = "PELAYA"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 459
  s.city_name  = "PUEBLO BELLO"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 460
  s.city_name  = "RÍO DE ORO"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 461
  s.city_name  = "LA PAZ"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 462
  s.city_name  = "SAN ALBERTO"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 463
  s.city_name  = "SAN DIEGO"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 464
  s.city_name  = "SAN MARTÍN"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 465
  s.city_name  = "TAMALAMEQUE"
  s.department_id  = 12
end
City.seed do |s|
  s.id    = 466
  s.city_name  = "QUIBDÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 467
  s.city_name  = "ACANDÍ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 468
  s.city_name  = "ALTO BAUDÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 469
  s.city_name  = "ATRATO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 470
  s.city_name  = "BAGADÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 471
  s.city_name  = "BAHÍA SOLANO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 472
  s.city_name  = "BAJO BAUDÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 473
  s.city_name  = "BOJAYA"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 474
  s.city_name  = "EL CANTÓN DEL SAN PABLO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 475
  s.city_name  = "CARMEN DEL DARIÉN"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 476
  s.city_name  = "CÉRTEGUI"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 477
  s.city_name  = "CONDOTO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 478
  s.city_name  = "EL CARMEN DE ATRATO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 479
  s.city_name  = "EL LITORAL DEL SAN JUAN"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 480
  s.city_name  = "ISTMINA"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 481
  s.city_name  = "JURADÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 482
  s.city_name  = "LLORÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 483
  s.city_name  = "MEDIO ATRATO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 484
  s.city_name  = "MEDIO BAUDÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 485
  s.city_name  = "MEDIO SAN JUAN"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 486
  s.city_name  = "NÓVITA"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 487
  s.city_name  = "NUQUÍ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 488
  s.city_name  = "RÍO IRÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 489
  s.city_name  = "RÍO QUITO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 490
  s.city_name  = "RIOSUCIO"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 491
  s.city_name  = "SAN JOSÉ DEL PALMAR"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 492
  s.city_name  = "SIPÍ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 493
  s.city_name  = "TADÓ"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 494
  s.city_name  = "UNGUÍA"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 495
  s.city_name  = "UNIÓN PANAMERICANA"
  s.department_id  = 13
end
City.seed do |s|
  s.id    = 496
  s.city_name  = "MONTERÍA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 497
  s.city_name  = "AYAPEL"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 498
  s.city_name  = "BUENAVISTA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 499
  s.city_name  = "CANALETE"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 500
  s.city_name  = "CERETÉ"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 501
  s.city_name  = "CHIMÁ"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 502
  s.city_name  = "CHINÚ"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 503
  s.city_name  = "CIÉNAGA DE ORO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 504
  s.city_name  = "COTORRA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 505
  s.city_name  = "LA APARTADA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 506
  s.city_name  = "LORICA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 507
  s.city_name  = "LOS CÓRDOBAS"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 508
  s.city_name  = "MOMIL"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 509
  s.city_name  = "MONTELÍBANO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 510
  s.city_name  = "MOÑITOS"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 511
  s.city_name  = "PLANETA RICA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 512
  s.city_name  = "PUEBLO NUEVO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 513
  s.city_name  = "PUERTO ESCONDIDO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 514
  s.city_name  = "PUERTO LIBERTADOR"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 515
  s.city_name  = "PURÍSIMA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 516
  s.city_name  = "SAHAGÚN"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 517
  s.city_name  = "SAN ANDRÉS SOTAVENTO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 518
  s.city_name  = "SAN ANTERO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 519
  s.city_name  = "SAN BERNARDO DEL VIENTO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 520
  s.city_name  = "SAN CARLOS"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 521
  s.city_name  = "SAN JOSÉ DE URÉ"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 522
  s.city_name  = "SAN PELAYO"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 523
  s.city_name  = "TIERRALTA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 524
  s.city_name  = "TUCHÍN"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 525
  s.city_name  = "VALENCIA"
  s.department_id  = 14
end
City.seed do |s|
  s.id    = 526
  s.city_name  = "AGUA DE DIOS"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 527
  s.city_name  = "ALBÁN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 528
  s.city_name  = "ANAPOIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 529
  s.city_name  = "ANOLAIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 530
  s.city_name  = "ARBELÁEZ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 531
  s.city_name  = "BELTRÁN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 532
  s.city_name  = "BITUIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 533
  s.city_name  = "BOJACÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 534
  s.city_name  = "CABRERA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 535
  s.city_name  = "CACHIPAY"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 536
  s.city_name  = "CAJICÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 537
  s.city_name  = "CAPARRAPÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 538
  s.city_name  = "CAQUEZA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 539
  s.city_name  = "CARMEN DE CARUPA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 540
  s.city_name  = "CHAGUANÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 541
  s.city_name  = "CHÍA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 542
  s.city_name  = "CHIPAQUE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 543
  s.city_name  = "CHOACHÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 544
  s.city_name  = "CHOCONTÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 545
  s.city_name  = "COGUA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 546
  s.city_name  = "COTA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 547
  s.city_name  = "CUCUNUBÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 548
  s.city_name  = "EL COLEGIO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 549
  s.city_name  = "EL PEÑÓN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 550
  s.city_name  = "EL ROSAL"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 551
  s.city_name  = "FACATATIVÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 552
  s.city_name  = "FOMEQUE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 553
  s.city_name  = "FOSCA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 554
  s.city_name  = "FUNZA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 555
  s.city_name  = "FÚQUENE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 556
  s.city_name  = "FUSAGASUGÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 557
  s.city_name  = "GACHALA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 558
  s.city_name  = "GACHANCIPÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 559
  s.city_name  = "GACHETÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 560
  s.city_name  = "GAMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 561
  s.city_name  = "GIRARDOT"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 562
  s.city_name  = "GRANADA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 563
  s.city_name  = "GUACHETÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 564
  s.city_name  = "GUADUAS"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 565
  s.city_name  = "GUASCA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 566
  s.city_name  = "GUATAQUÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 567
  s.city_name  = "GUATAVITA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 568
  s.city_name  = "GUAYABAL DE SIQUIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 569
  s.city_name  = "GUAYABETAL"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 570
  s.city_name  = "GUTIÉRREZ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 571
  s.city_name  = "JERUSALÉN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 572
  s.city_name  = "JUNÍN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 573
  s.city_name  = "LA CALERA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 574
  s.city_name  = "LA MESA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 575
  s.city_name  = "LA PALMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 576
  s.city_name  = "LA PEÑA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 577
  s.city_name  = "LA VEGA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 578
  s.city_name  = "LENGUAZAQUE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 579
  s.city_name  = "MACHETA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 580
  s.city_name  = "MADRID"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 581
  s.city_name  = "MANTA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 582
  s.city_name  = "MEDINA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 583
  s.city_name  = "MOSQUERA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 584
  s.city_name  = "NARIÑO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 585
  s.city_name  = "NEMOCÓN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 586
  s.city_name  = "NILO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 587
  s.city_name  = "NIMAIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 588
  s.city_name  = "NOCAIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 589
  s.city_name  = "VENECIA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 590
  s.city_name  = "PACHO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 591
  s.city_name  = "PAIME"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 592
  s.city_name  = "PANDI"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 593
  s.city_name  = "PARATEBUENO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 594
  s.city_name  = "PASCA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 595
  s.city_name  = "PUERTO SALGAR"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 596
  s.city_name  = "PULÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 597
  s.city_name  = "QUEBRADANEGRA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 598
  s.city_name  = "QUETAME"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 599
  s.city_name  = "QUIPILE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 600
  s.city_name  = "APULO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 601
  s.city_name  = "RICAURTE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 602
  s.city_name  = "SAN ANTONIO DEL TEQUENDAMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 603
  s.city_name  = "SAN BERNARDO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 604
  s.city_name  = "SAN CAYETANO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 605
  s.city_name  = "SAN FRANCISCO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 606
  s.city_name  = "SAN JUAN DE RÍO SECO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 607
  s.city_name  = "SASAIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 608
  s.city_name  = "SESQUILÉ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 609
  s.city_name  = "SIBATÉ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 610
  s.city_name  = "SILVANIA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 611
  s.city_name  = "SIMIJACA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 612
  s.city_name  = "SOACHA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 613
  s.city_name  = "SOPÓ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 614
  s.city_name  = "SUBACHOQUE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 615
  s.city_name  = "SUESCA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 616
  s.city_name  = "SUPATÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 617
  s.city_name  = "SUSA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 618
  s.city_name  = "SUTATAUSA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 619
  s.city_name  = "TABIO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 620
  s.city_name  = "TAUSA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 621
  s.city_name  = "TENA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 622
  s.city_name  = "TENJO"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 623
  s.city_name  = "TIBACUY"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 624
  s.city_name  = "TIBIRITA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 625
  s.city_name  = "TOCAIMA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 626
  s.city_name  = "TOCANCIPÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 627
  s.city_name  = "TOPAIPÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 628
  s.city_name  = "UBALÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 629
  s.city_name  = "UBAQUE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 630
  s.city_name  = "VILLA DE SAN DIEGO DE UBATE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 631
  s.city_name  = "UNE"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 632
  s.city_name  = "ÚTICA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 633
  s.city_name  = "VERGARA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 634
  s.city_name  = "VIANÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 635
  s.city_name  = "VILLAGÓMEZ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 636
  s.city_name  = "VILLAPINZÓN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 637
  s.city_name  = "VILLETA"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 638
  s.city_name  = "VIOTÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 639
  s.city_name  = "YACOPÍ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 640
  s.city_name  = "ZIPACÓN"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 641
  s.city_name  = "ZIPAQUIRÁ"
  s.department_id  = 15
end
City.seed do |s|
  s.id    = 642
  s.city_name  = "INÍRIDA"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 643
  s.city_name  = "BARRANCO MINAS"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 644
  s.city_name  = "MAPIRIPANA"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 645
  s.city_name  = "SAN FELIPE"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 646
  s.city_name  = "PUERTO COLOMBIA"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 647
  s.city_name  = "LA GUADALUPE"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 648
  s.city_name  = "CACAHUAL"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 649
  s.city_name  = "PANA PANA"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 650
  s.city_name  = "MORICHAL"
  s.department_id  = 16
end
City.seed do |s|
  s.id    = 651
  s.city_name  = "SAN JOSÉ DEL GUAVIARE"
  s.department_id  = 17
end
City.seed do |s|
  s.id    = 652
  s.city_name  = "CALAMAR"
  s.department_id  = 17
end
City.seed do |s|
  s.id    = 653
  s.city_name  = "EL RETORNO"
  s.department_id  = 17
end
City.seed do |s|
  s.id    = 654
  s.city_name  = "MIRAFLORES"
  s.department_id  = 17
end
City.seed do |s|
  s.id    = 655
  s.city_name  = "NEIVA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 656
  s.city_name  = "ACEVEDO"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 657
  s.city_name  = "AGRADO"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 658
  s.city_name  = "AIPE"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 659
  s.city_name  = "ALGECIRAS"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 660
  s.city_name  = "ALTAMIRA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 661
  s.city_name  = "BARAYA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 662
  s.city_name  = "CAMPOALEGRE"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 663
  s.city_name  = "COLOMBIA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 664
  s.city_name  = "ELÍAS"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 665
  s.city_name  = "GARZÓN"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 666
  s.city_name  = "GIGANTE"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 667
  s.city_name  = "GUADALUPE"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 668
  s.city_name  = "HOBO"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 669
  s.city_name  = "IQUIRA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 670
  s.city_name  = "ISNOS"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 671
  s.city_name  = "LA ARGENTINA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 672
  s.city_name  = "LA PLATA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 673
  s.city_name  = "NÁTAGA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 674
  s.city_name  = "OPORAPA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 675
  s.city_name  = "PAICOL"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 676
  s.city_name  = "PALERMO"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 677
  s.city_name  = "PALESTINA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 678
  s.city_name  = "PITAL"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 679
  s.city_name  = "PITALITO"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 680
  s.city_name  = "RIVERA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 681
  s.city_name  = "SALADOBLANCO"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 682
  s.city_name  = "SAN AGUSTÍN"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 683
  s.city_name  = "SANTA MARÍA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 684
  s.city_name  = "SUAZA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 685
  s.city_name  = "TARQUI"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 686
  s.city_name  = "TESALIA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 687
  s.city_name  = "TELLO"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 688
  s.city_name  = "TERUEL"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 689
  s.city_name  = "TIMANÁ"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 690
  s.city_name  = "VILLAVIEJA"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 691
  s.city_name  = "YAGUARÁ"
  s.department_id  = 18
end
City.seed do |s|
  s.id    = 692
  s.city_name  = "RIOHACHA"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 693
  s.city_name  = "ALBANIA"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 694
  s.city_name  = "BARRANCAS"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 695
  s.city_name  = "DIBULLA"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 696
  s.city_name  = "DISTRACCIÓN"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 697
  s.city_name  = "EL MOLINO"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 698
  s.city_name  = "FONSECA"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 699
  s.city_name  = "HATONUEVO"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 700
  s.city_name  = "LA JAGUA DEL PILAR"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 701
  s.city_name  = "MAICAO"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 702
  s.city_name  = "MANAURE"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 703
  s.city_name  = "SAN JUAN DEL CESAR"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 704
  s.city_name  = "URIBIA"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 705
  s.city_name  = "URUMITA"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 706
  s.city_name  = "VILLANUEVA"
  s.department_id  = 19
end
City.seed do |s|
  s.id    = 707
  s.city_name  = "SANTA MARTA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 708
  s.city_name  = "ALGARROBO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 709
  s.city_name  = "ARACATACA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 710
  s.city_name  = "ARIGUANÍ"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 711
  s.city_name  = "CERRO SAN ANTONIO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 712
  s.city_name  = "CHIVOLO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 713
  s.city_name  = "CIÉNAGA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 714
  s.city_name  = "CONCORDIA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 715
  s.city_name  = "EL BANCO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 716
  s.city_name  = "EL PIÑON"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 717
  s.city_name  = "EL RETÉN"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 718
  s.city_name  = "FUNDACIÓN"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 719
  s.city_name  = "GUAMAL"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 720
  s.city_name  = "NUEVA GRANADA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 721
  s.city_name  = "PEDRAZA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 722
  s.city_name  = "PIJIÑO DEL CARMEN"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 723
  s.city_name  = "PIVIJAY"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 724
  s.city_name  = "PLATO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 725
  s.city_name  = "PUEBLOVIEJO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 726
  s.city_name  = "REMOLINO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 727
  s.city_name  = "SABANAS DE SAN ANGEL"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 728
  s.city_name  = "SALAMINA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 729
  s.city_name  = "SAN SEBASTIÁN DE BUENAVISTA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 730
  s.city_name  = "SAN ZENÓN"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 731
  s.city_name  = "SANTA ANA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 732
  s.city_name  = "SANTA BÁRBARA DE PINTO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 733
  s.city_name  = "SITIONUEVO"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 734
  s.city_name  = "TENERIFE"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 735
  s.city_name  = "ZAPAYÁN"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 736
  s.city_name  = "ZONA BANANERA"
  s.department_id  = 20
end
City.seed do |s|
  s.id    = 737
  s.city_name  = "VILLAVICENCIO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 738
  s.city_name  = "ACACÍAS"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 739
  s.city_name  = "BARRANCA DE UPÍA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 740
  s.city_name  = "CABUYARO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 741
  s.city_name  = "CASTILLA LA NUEVA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 742
  s.city_name  = "CUBARRAL"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 743
  s.city_name  = "CUMARAL"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 744
  s.city_name  = "EL CALVARIO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 745
  s.city_name  = "EL CASTILLO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 746
  s.city_name  = "EL DORADO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 747
  s.city_name  = "FUENTE DE ORO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 748
  s.city_name  = "GRANADA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 749
  s.city_name  = "GUAMAL"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 750
  s.city_name  = "MAPIRIPÁN"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 751
  s.city_name  = "MESETAS"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 752
  s.city_name  = "LA MACARENA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 753
  s.city_name  = "URIBE"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 754
  s.city_name  = "LEJANÍAS"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 755
  s.city_name  = "PUERTO CONCORDIA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 756
  s.city_name  = "PUERTO GAITÁN"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 757
  s.city_name  = "PUERTO LÓPEZ"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 758
  s.city_name  = "PUERTO LLERAS"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 759
  s.city_name  = "PUERTO RICO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 760
  s.city_name  = "RESTREPO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 761
  s.city_name  = "SAN CARLOS DE GUAROA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 762
  s.city_name  = "SAN JUAN DE ARAMA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 763
  s.city_name  = "SAN JUANITO"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 764
  s.city_name  = "SAN MARTÍN"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 765
  s.city_name  = "VISTAHERMOSA"
  s.department_id  = 21
end
City.seed do |s|
  s.id    = 766
  s.city_name  = "PASTO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 767
  s.city_name  = "ALBÁN"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 768
  s.city_name  = "ALDANA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 769
  s.city_name  = "ANCUYÁ"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 770
  s.city_name  = "ARBOLEDA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 771
  s.city_name  = "BARBACOAS"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 772
  s.city_name  = "BELÉN"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 773
  s.city_name  = "BUESACO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 774
  s.city_name  = "COLÓN"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 775
  s.city_name  = "CONSACA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 776
  s.city_name  = "CONTADERO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 777
  s.city_name  = "CÓRDOBA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 778
  s.city_name  = "CUASPUD"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 779
  s.city_name  = "CUMBAL"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 780
  s.city_name  = "CUMBITARA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 781
  s.city_name  = "CHACHAGÜÍ"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 782
  s.city_name  = "EL CHARCO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 783
  s.city_name  = "EL PEÑOL"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 784
  s.city_name  = "EL ROSARIO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 785
  s.city_name  = "EL TABLÓN DE GÓMEZ"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 786
  s.city_name  = "EL TAMBO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 787
  s.city_name  = "FUNES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 788
  s.city_name  = "GUACHUCAL"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 789
  s.city_name  = "GUAITARILLA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 790
  s.city_name  = "GUALMATÁN"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 791
  s.city_name  = "ILES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 792
  s.city_name  = "IMUÉS"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 793
  s.city_name  = "IPIALES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 794
  s.city_name  = "LA CRUZ"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 795
  s.city_name  = "LA FLORIDA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 796
  s.city_name  = "LA LLANADA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 797
  s.city_name  = "LA TOLA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 798
  s.city_name  = "LA UNIÓN"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 799
  s.city_name  = "LEIVA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 800
  s.city_name  = "LINARES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 801
  s.city_name  = "LOS ANDES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 802
  s.city_name  = "MAGÜI"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 803
  s.city_name  = "MALLAMA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 804
  s.city_name  = "MOSQUERA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 805
  s.city_name  = "NARIÑO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 806
  s.city_name  = "OLAYA HERRERA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 807
  s.city_name  = "OSPINA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 808
  s.city_name  = "FRANCISCO PIZARRO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 809
  s.city_name  = "POLICARPA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 810
  s.city_name  = "POTOSÍ"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 811
  s.city_name  = "PROVIDENCIA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 812
  s.city_name  = "PUERRES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 813
  s.city_name  = "PUPIALES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 814
  s.city_name  = "RICAURTE"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 815
  s.city_name  = "ROBERTO PAYÁN"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 816
  s.city_name  = "SAMANIEGO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 817
  s.city_name  = "SANDONÁ"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 818
  s.city_name  = "SAN BERNARDO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 819
  s.city_name  = "SAN LORENZO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 820
  s.city_name  = "SAN PABLO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 821
  s.city_name  = "SAN PEDRO DE CARTAGO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 822
  s.city_name  = "SANTA BÁRBARA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 823
  s.city_name  = "SANTACRUZ"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 824
  s.city_name  = "SAPUYES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 825
  s.city_name  = "TAMINANGO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 826
  s.city_name  = "TANGUA"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 827
  s.city_name  = "SAN ANDRES DE TUMACO"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 828
  s.city_name  = "TÚQUERRES"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 829
  s.city_name  = "YACUANQUER"
  s.department_id  = 22
end
City.seed do |s|
  s.id    = 830
  s.city_name  = "CÚCUTA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 831
  s.city_name  = "ABREGO"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 832
  s.city_name  = "ARBOLEDAS"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 833
  s.city_name  = "BOCHALEMA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 834
  s.city_name  = "BUCARASICA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 835
  s.city_name  = "CÁCOTA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 836
  s.city_name  = "CACHIRÁ"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 837
  s.city_name  = "CHINÁCOTA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 838
  s.city_name  = "CHITAGÁ"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 839
  s.city_name  = "CONVENCIÓN"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 840
  s.city_name  = "CUCUTILLA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 841
  s.city_name  = "DURANIA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 842
  s.city_name  = "EL CARMEN"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 843
  s.city_name  = "EL TARRA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 844
  s.city_name  = "EL ZULIA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 845
  s.city_name  = "GRAMALOTE"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 846
  s.city_name  = "HACARÍ"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 847
  s.city_name  = "HERRÁN"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 848
  s.city_name  = "LABATECA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 849
  s.city_name  = "LA ESPERANZA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 850
  s.city_name  = "LA PLAYA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 851
  s.city_name  = "LOS PATIOS"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 852
  s.city_name  = "LOURDES"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 853
  s.city_name  = "MUTISCUA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 854
  s.city_name  = "OCAÑA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 855
  s.city_name  = "PAMPLONA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 856
  s.city_name  = "PAMPLONITA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 857
  s.city_name  = "PUERTO SANTANDER"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 858
  s.city_name  = "RAGONVALIA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 859
  s.city_name  = "SALAZAR"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 860
  s.city_name  = "SAN CALIXTO"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 861
  s.city_name  = "SAN CAYETANO"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 862
  s.city_name  = "SANTIAGO"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 863
  s.city_name  = "SARDINATA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 864
  s.city_name  = "SILOS"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 865
  s.city_name  = "TEORAMA"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 866
  s.city_name  = "TIBÚ"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 867
  s.city_name  = "TOLEDO"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 868
  s.city_name  = "VILLA CARO"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 869
  s.city_name  = "VILLA DEL ROSARIO"
  s.department_id  = 23
end
City.seed do |s|
  s.id    = 870
  s.city_name  = "MOCOA"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 871
  s.city_name  = "COLÓN"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 872
  s.city_name  = "ORITO"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 873
  s.city_name  = "PUERTO ASÍS"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 874
  s.city_name  = "PUERTO CAICEDO"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 875
  s.city_name  = "PUERTO GUZMÁN"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 876
  s.city_name  = "PUERTO LEGUÍZAMO"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 877
  s.city_name  = "SIBUNDOY"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 878
  s.city_name  = "SAN FRANCISCO"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 879
  s.city_name  = "SAN MIGUEL"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 880
  s.city_name  = "SANTIAGO"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 881
  s.city_name  = "VALLE DEL GUAMUEZ"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 882
  s.city_name  = "VILLAGARZÓN"
  s.department_id  = 24
end
City.seed do |s|
  s.id    = 883
  s.city_name  = "ARMENIA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 884
  s.city_name  = "BUENAVISTA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 885
  s.city_name  = "CALARCA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 886
  s.city_name  = "CIRCASIA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 887
  s.city_name  = "CÓRDOBA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 888
  s.city_name  = "FILANDIA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 889
  s.city_name  = "GÉNOVA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 890
  s.city_name  = "LA TEBAIDA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 891
  s.city_name  = "MONTENEGRO"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 892
  s.city_name  = "PIJAO"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 893
  s.city_name  = "QUIMBAYA"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 894
  s.city_name  = "SALENTO"
  s.department_id  = 25
end
City.seed do |s|
  s.id    = 895
  s.city_name  = "PEREIRA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 896
  s.city_name  = "APÍA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 897
  s.city_name  = "BALBOA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 898
  s.city_name  = "BELÉN DE UMBRÍA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 899
  s.city_name  = "DOSQUEBRADAS"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 900
  s.city_name  = "GUÁTICA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 901
  s.city_name  = "LA CELIA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 902
  s.city_name  = "LA VIRGINIA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 903
  s.city_name  = "MARSELLA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 904
  s.city_name  = "MISTRATÓ"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 905
  s.city_name  = "PUEBLO RICO"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 906
  s.city_name  = "QUINCHÍA"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 907
  s.city_name  = "SANTA ROSA DE CABAL"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 908
  s.city_name  = "SANTUARIO"
  s.department_id  = 26
end
City.seed do |s|
  s.id    = 909
  s.city_name  = "SAN ANDRÉS"
  s.department_id  = 27
end
City.seed do |s|
  s.id    = 910
  s.city_name  = "PROVIDENCIA"
  s.department_id  = 27
end
City.seed do |s|
  s.id    = 911
  s.city_name  = "BUCARAMANGA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 912
  s.city_name  = "AGUADA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 913
  s.city_name  = "ALBANIA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 914
  s.city_name  = "ARATOCA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 915
  s.city_name  = "BARBOSA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 916
  s.city_name  = "BARICHARA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 917
  s.city_name  = "BARRANCABERMEJA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 918
  s.city_name  = "BETULIA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 919
  s.city_name  = "BOLÍVAR"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 920
  s.city_name  = "CABRERA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 921
  s.city_name  = "CALIFORNIA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 922
  s.city_name  = "CAPITANEJO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 923
  s.city_name  = "CARCASÍ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 924
  s.city_name  = "CEPITÁ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 925
  s.city_name  = "CERRITO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 926
  s.city_name  = "CHARALÁ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 927
  s.city_name  = "CHARTA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 928
  s.city_name  = "CHIMA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 929
  s.city_name  = "CHIPATÁ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 930
  s.city_name  = "CIMITARRA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 931
  s.city_name  = "CONCEPCIÓN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 932
  s.city_name  = "CONFINES"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 933
  s.city_name  = "CONTRATACIÓN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 934
  s.city_name  = "COROMORO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 935
  s.city_name  = "CURITÍ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 936
  s.city_name  = "EL CARMEN DE CHUCURÍ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 937
  s.city_name  = "EL GUACAMAYO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 938
  s.city_name  = "EL PEÑÓN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 939
  s.city_name  = "EL PLAYÓN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 940
  s.city_name  = "ENCINO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 941
  s.city_name  = "ENCISO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 942
  s.city_name  = "FLORIÁN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 943
  s.city_name  = "FLORIDABLANCA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 944
  s.city_name  = "GALÁN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 945
  s.city_name  = "GAMBITA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 946
  s.city_name  = "GIRÓN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 947
  s.city_name  = "GUACA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 948
  s.city_name  = "GUADALUPE"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 949
  s.city_name  = "GUAPOTÁ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 950
  s.city_name  = "GUAVATÁ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 951
  s.city_name  = "GÜEPSA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 952
  s.city_name  = "HATO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 953
  s.city_name  = "JESÚS MARÍA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 954
  s.city_name  = "JORDÁN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 955
  s.city_name  = "LA BELLEZA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 956
  s.city_name  = "LANDÁZURI"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 957
  s.city_name  = "LA PAZ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 958
  s.city_name  = "LEBRIJA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 959
  s.city_name  = "LOS SANTOS"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 960
  s.city_name  = "MACARAVITA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 961
  s.city_name  = "MÁLAGA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 962
  s.city_name  = "MATANZA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 963
  s.city_name  = "MOGOTES"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 964
  s.city_name  = "MOLAGAVITA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 965
  s.city_name  = "OCAMONTE"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 966
  s.city_name  = "OIBA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 967
  s.city_name  = "ONZAGA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 968
  s.city_name  = "PALMAR"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 969
  s.city_name  = "PALMAS DEL SOCORRO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 970
  s.city_name  = "PÁRAMO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 971
  s.city_name  = "PIEDECUESTA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 972
  s.city_name  = "PINCHOTE"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 973
  s.city_name  = "PUENTE NACIONAL"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 974
  s.city_name  = "PUERTO PARRA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 975
  s.city_name  = "PUERTO WILCHES"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 976
  s.city_name  = "RIONEGRO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 977
  s.city_name  = "SABANA DE TORRES"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 978
  s.city_name  = "SAN ANDRÉS"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 979
  s.city_name  = "SAN BENITO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 980
  s.city_name  = "SAN GIL"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 981
  s.city_name  = "SAN JOAQUÍN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 982
  s.city_name  = "SAN JOSÉ DE MIRANDA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 983
  s.city_name  = "SAN MIGUEL"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 984
  s.city_name  = "SAN VICENTE DE CHUCURÍ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 985
  s.city_name  = "SANTA BÁRBARA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 986
  s.city_name  = "SANTA HELENA DEL OPÓN"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 987
  s.city_name  = "SIMACOTA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 988
  s.city_name  = "SOCORRO"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 989
  s.city_name  = "SUAITA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 990
  s.city_name  = "SUCRE"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 991
  s.city_name  = "SURATÁ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 992
  s.city_name  = "TONA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 993
  s.city_name  = "VALLE DE SAN JOSÉ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 994
  s.city_name  = "VÉLEZ"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 995
  s.city_name  = "VETAS"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 996
  s.city_name  = "VILLANUEVA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 997
  s.city_name  = "ZAPATOCA"
  s.department_id  = 28
end
City.seed do |s|
  s.id    = 998
  s.city_name  = "SINCELEJO"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 999
  s.city_name  = "BUENAVISTA"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1000
  s.city_name  = "CAIMITO"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1001
  s.city_name  = "COLOSO"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1002
  s.city_name  = "COROZAL"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1003
  s.city_name  = "COVEÑAS"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1004
  s.city_name  = "CHALÁN"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1005
  s.city_name  = "EL ROBLE"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1006
  s.city_name  = "GALERAS"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1007
  s.city_name  = "GUARANDA"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1008
  s.city_name  = "LA UNIÓN"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1009
  s.city_name  = "LOS PALMITOS"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1010
  s.city_name  = "MAJAGUAL"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1011
  s.city_name  = "MORROA"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1012
  s.city_name  = "OVEJAS"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1013
  s.city_name  = "PALMITO"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1014
  s.city_name  = "SAMPUÉS"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1015
  s.city_name  = "SAN BENITO ABAD"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1016
  s.city_name  = "SAN JUAN DE BETULIA"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1017
  s.city_name  = "SAN MARCOS"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1018
  s.city_name  = "SAN ONOFRE"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1019
  s.city_name  = "SAN PEDRO"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1020
  s.city_name  = "SAN LUIS DE SINCÉ"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1021
  s.city_name  = "SUCRE"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1022
  s.city_name  = "SANTIAGO DE TOLÚ"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1023
  s.city_name  = "TOLÚ VIEJO"
  s.department_id  = 29
end
City.seed do |s|
  s.id    = 1024
  s.city_name  = "IBAGUÉ"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1025
  s.city_name  = "ALPUJARRA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1026
  s.city_name  = "ALVARADO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1027
  s.city_name  = "AMBALEMA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1028
  s.city_name  = "ANZOÁTEGUI"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1029
  s.city_name  = "ARMERO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1030
  s.city_name  = "ATACO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1031
  s.city_name  = "CAJAMARCA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1032
  s.city_name  = "CARMEN DE APICALÁ"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1033
  s.city_name  = "CASABIANCA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1034
  s.city_name  = "CHAPARRAL"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1035
  s.city_name  = "COELLO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1036
  s.city_name  = "COYAIMA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1037
  s.city_name  = "CUNDAY"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1038
  s.city_name  = "DOLORES"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1039
  s.city_name  = "ESPINAL"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1040
  s.city_name  = "FALAN"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1041
  s.city_name  = "FLANDES"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1042
  s.city_name  = "FRESNO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1043
  s.city_name  = "GUAMO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1044
  s.city_name  = "HERVEO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1045
  s.city_name  = "HONDA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1046
  s.city_name  = "ICONONZO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1047
  s.city_name  = "LÉRIDA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1048
  s.city_name  = "LÍBANO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1049
  s.city_name  = "SAN SEBASTIÁN DE MARIQUITA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1050
  s.city_name  = "MELGAR"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1051
  s.city_name  = "MURILLO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1052
  s.city_name  = "NATAGAIMA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1053
  s.city_name  = "ORTEGA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1054
  s.city_name  = "PALOCABILDO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1055
  s.city_name  = "PIEDRAS"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1056
  s.city_name  = "PLANADAS"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1057
  s.city_name  = "PRADO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1058
  s.city_name  = "PURIFICACIÓN"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1059
  s.city_name  = "RIOBLANCO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1060
  s.city_name  = "RONCESVALLES"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1061
  s.city_name  = "ROVIRA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1062
  s.city_name  = "SALDAÑA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1063
  s.city_name  = "SAN ANTONIO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1064
  s.city_name  = "SAN LUIS"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1065
  s.city_name  = "SANTA ISABEL"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1066
  s.city_name  = "SUÁREZ"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1067
  s.city_name  = "VALLE DE SAN JUAN"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1068
  s.city_name  = "VENADILLO"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1069
  s.city_name  = "VILLAHERMOSA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1070
  s.city_name  = "VILLARRICA"
  s.department_id  = 30
end
City.seed do |s|
  s.id    = 1071
  s.city_name  = "CALI"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1072
  s.city_name  = "ALCALÁ"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1073
  s.city_name  = "ANDALUCÍA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1074
  s.city_name  = "ANSERMANUEVO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1075
  s.city_name  = "ARGELIA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1076
  s.city_name  = "BOLÍVAR"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1077
  s.city_name  = "BUENAVENTURA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1078
  s.city_name  = "GUADALAJARA DE BUGA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1079
  s.city_name  = "BUGALAGRANDE"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1080
  s.city_name  = "CAICEDONIA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1081
  s.city_name  = "CALIMA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1082
  s.city_name  = "CANDELARIA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1083
  s.city_name  = "CARTAGO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1084
  s.city_name  = "DAGUA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1085
  s.city_name  = "EL ÁGUILA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1086
  s.city_name  = "EL CAIRO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1087
  s.city_name  = "EL CERRITO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1088
  s.city_name  = "EL DOVIO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1089
  s.city_name  = "FLORIDA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1090
  s.city_name  = "GINEBRA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1091
  s.city_name  = "GUACARÍ"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1092
  s.city_name  = "JAMUNDÍ"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1093
  s.city_name  = "LA CUMBRE"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1094
  s.city_name  = "LA UNIÓN"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1095
  s.city_name  = "LA VICTORIA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1096
  s.city_name  = "OBANDO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1097
  s.city_name  = "PALMIRA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1098
  s.city_name  = "PRADERA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1099
  s.city_name  = "RESTREPO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1100
  s.city_name  = "RIOFRÍO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1101
  s.city_name  = "ROLDANILLO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1102
  s.city_name  = "SAN PEDRO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1103
  s.city_name  = "SEVILLA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1104
  s.city_name  = "TORO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1105
  s.city_name  = "TRUJILLO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1106
  s.city_name  = "TULUÁ"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1107
  s.city_name  = "ULLOA"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1108
  s.city_name  = "VERSALLES"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1109
  s.city_name  = "VIJES"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1110
  s.city_name  = "YOTOCO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1111
  s.city_name  = "YUMBO"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1112
  s.city_name  = "ZARZAL"
  s.department_id  = 31
end
City.seed do |s|
  s.id    = 1113
  s.city_name  = "MITÚ"
  s.department_id  = 32
end
City.seed do |s|
  s.id    = 1114
  s.city_name  = "CARURU"
  s.department_id  = 32
end
City.seed do |s|
  s.id    = 1115
  s.city_name  = "PACOA"
  s.department_id  = 32
end
City.seed do |s|
  s.id    = 1116
  s.city_name  = "TARAIRA"
  s.department_id  = 32
end
City.seed do |s|
  s.id    = 1117
  s.city_name  = "PAPUNAUA"
  s.department_id  = 32
end
City.seed do |s|
  s.id    = 1118
  s.city_name  = "YAVARATÉ"
  s.department_id  = 32
end
City.seed do |s|
  s.id    = 1119
  s.city_name  = "PUERTO CARREÑO"
  s.department_id  = 33
end
City.seed do |s|
  s.id    = 1120
  s.city_name  = "LA PRIMAVERA"
  s.department_id  = 33
end
City.seed do |s|
  s.id    = 1121
  s.city_name  = "SANTA ROSALÍA"
  s.department_id  = 33
end
City.seed do |s|
  s.id    = 1122
  s.city_name  = "CUMARIBO"
  s.department_id  = 33
end
