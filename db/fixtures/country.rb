Country.seed do |s|
  s.id    = 1
  s.country_name  = "Angola"
end
Country.seed do |s|
  s.id    = 2
  s.country_name  = "Antigua y Barbuda"
end
Country.seed do |s|
  s.id    = 3
  s.country_name  = "Arabia Saudita"
end
Country.seed do |s|
  s.id    = 4
  s.country_name  = "Argelia"
end
Country.seed do |s|
  s.id    = 5
  s.country_name  = "Argentina"
end
Country.seed do |s|
  s.id    = 6
  s.country_name  = "Armenia"
end
Country.seed do |s|
  s.id    = 7
  s.country_name  = "Australia"
end
Country.seed do |s|
  s.id    = 8
  s.country_name  = "Austria"
end
Country.seed do |s|
  s.id    = 9
  s.country_name  = "Azerbaiyán"
end
Country.seed do |s|
  s.id    = 10
  s.country_name  = "Bahamas"
end
Country.seed do |s|
  s.id    = 11
  s.country_name  = "Bangladés"
end
Country.seed do |s|
  s.id    = 12
  s.country_name  = "Barbados"
end
Country.seed do |s|
  s.id    = 13
  s.country_name  = "Baréin"
end
Country.seed do |s|
  s.id    = 14
  s.country_name  = "Bélgica"
end
Country.seed do |s|
  s.id    = 15
  s.country_name  = "Belice"
end
Country.seed do |s|
  s.id    = 16
  s.country_name  = "Benín"
end
Country.seed do |s|
  s.id    = 17
  s.country_name  = "Bielorrusia"
end
Country.seed do |s|
  s.id    = 18
  s.country_name  = "Birmania"
end
Country.seed do |s|
  s.id    = 19
  s.country_name  = "Bolivia"
end
Country.seed do |s|
  s.id    = 20
  s.country_name  = "Bosnia y Herzegovina"
end
Country.seed do |s|
  s.id    = 21
  s.country_name  = "Botsuana"
end
Country.seed do |s|
  s.id    = 22
  s.country_name  = "Brasil"
end
Country.seed do |s|
  s.id    = 23
  s.country_name  = "Brunéi"
end
Country.seed do |s|
  s.id    = 24
  s.country_name  = "Bulgaria"
end
Country.seed do |s|
  s.id    = 25
  s.country_name  = "Burkina Faso"
end
Country.seed do |s|
  s.id    = 26
  s.country_name  = "Burundi"
end
Country.seed do |s|
  s.id    = 27
  s.country_name  = "Bután"
end
Country.seed do |s|
  s.id    = 28
  s.country_name  = "Cabo Verde"
end
Country.seed do |s|
  s.id    = 29
  s.country_name  = "Camboya"
end
Country.seed do |s|
  s.id    = 30
  s.country_name  = "Camerún"
end
Country.seed do |s|
  s.id    = 31
  s.country_name  = "Canadá"
end
Country.seed do |s|
  s.id    = 32
  s.country_name  = "Catar"
end
Country.seed do |s|
  s.id    = 33
  s.country_name  = "Chad"
end
Country.seed do |s|
  s.id    = 34
  s.country_name  = "Chile"
end
Country.seed do |s|
  s.id    = 35
  s.country_name  = "China"
end
Country.seed do |s|
  s.id    = 36
  s.country_name  = "Chipre"
end
Country.seed do |s|
  s.id    = 37
  s.country_name  = "Ciudad del Vaticano"
end
Country.seed do |s|
  s.id    = 38
  s.country_name  = "Colombia"
end
Country.seed do |s|
  s.id    = 39
  s.country_name  = "Comoras"
end
Country.seed do |s|
  s.id    = 40
  s.country_name  = "Corea del Norte"
end
Country.seed do |s|
  s.id    = 41
  s.country_name  = "Corea del Sur"
end
Country.seed do |s|
  s.id    = 42
  s.country_name  = "Costa de Marfil"
end
Country.seed do |s|
  s.id    = 43
  s.country_name  = "Costa Rica"
end
Country.seed do |s|
  s.id    = 44
  s.country_name  = "Croacia"
end
Country.seed do |s|
  s.id    = 45
  s.country_name  = "Cuba"
end
Country.seed do |s|
  s.id    = 46
  s.country_name  = "Dinamarca"
end
Country.seed do |s|
  s.id    = 47
  s.country_name  = "Dominica"
end
Country.seed do |s|
  s.id    = 48
  s.country_name  = "Ecuador"
end
Country.seed do |s|
  s.id    = 49
  s.country_name  = "Egipto"
end
Country.seed do |s|
  s.id    = 50
  s.country_name  = "El Salvador"
end
Country.seed do |s|
  s.id    = 51
  s.country_name  = "Emiratos Árabes Unidos"
end
Country.seed do |s|
  s.id    = 52
  s.country_name  = "Eritrea"
end
Country.seed do |s|
  s.id    = 53
  s.country_name  = "Eslovaquia"
end
Country.seed do |s|
  s.id    = 54
  s.country_name  = "Eslovenia"
end
Country.seed do |s|
  s.id    = 55
  s.country_name  = "España"
end
Country.seed do |s|
  s.id    = 56
  s.country_name  = "Estados Unidos"
end
Country.seed do |s|
  s.id    = 57
  s.country_name  = "Estonia"
end
Country.seed do |s|
  s.id    = 58
  s.country_name  = "Etiopía"
end
Country.seed do |s|
  s.id    = 59
  s.country_name  = "Filipinas"
end
Country.seed do |s|
  s.id    = 60
  s.country_name  = "Finlandia"
end
Country.seed do |s|
  s.id    = 61
  s.country_name  = "Fiyi"
end
Country.seed do |s|
  s.id    = 62
  s.country_name  = "Francia"
end
Country.seed do |s|
  s.id    = 63
  s.country_name  = "Gabón"
end
Country.seed do |s|
  s.id    = 64
  s.country_name  = "Gambia"
end
Country.seed do |s|
  s.id    = 65
  s.country_name  = "Georgia"
end
Country.seed do |s|
  s.id    = 66
  s.country_name  = "Ghana"
end
Country.seed do |s|
  s.id    = 67
  s.country_name  = "Granada"
end
Country.seed do |s|
  s.id    = 68
  s.country_name  = "Grecia"
end
Country.seed do |s|
  s.id    = 69
  s.country_name  = "Guatemala"
end
Country.seed do |s|
  s.id    = 70
  s.country_name  = "Guyana"
end
Country.seed do |s|
  s.id    = 71
  s.country_name  = "Guinea"
end
Country.seed do |s|
  s.id    = 72
  s.country_name  = "Guinea ecuatorial"
end
Country.seed do |s|
  s.id    = 73
  s.country_name  = "Guinea-Bisáu"
end
Country.seed do |s|
  s.id    = 74
  s.country_name  = "Haití"
end
Country.seed do |s|
  s.id    = 75
  s.country_name  = "Honduras"
end
Country.seed do |s|
  s.id    = 76
  s.country_name  = "Hungría"
end
Country.seed do |s|
  s.id    = 77
  s.country_name  = "India"
end
Country.seed do |s|
  s.id    = 78
  s.country_name  = "Indonesia"
end
Country.seed do |s|
  s.id    = 79
  s.country_name  = "Irak"
end
Country.seed do |s|
  s.id    = 80
  s.country_name  = "Irán"
end
Country.seed do |s|
  s.id    = 81
  s.country_name  = "Irlanda"
end
Country.seed do |s|
  s.id    = 82
  s.country_name  = "Islandia"
end
Country.seed do |s|
  s.id    = 83
  s.country_name  = "Islas Marshall"
end
Country.seed do |s|
  s.id    = 84
  s.country_name  = "Islas Salomón"
end
Country.seed do |s|
  s.id    = 85
  s.country_name  = "Israel"
end
Country.seed do |s|
  s.id    = 86
  s.country_name  = "Italia"
end
Country.seed do |s|
  s.id    = 87
  s.country_name  = "Jamaica"
end
Country.seed do |s|
  s.id    = 88
  s.country_name  = "Japón"
end
Country.seed do |s|
  s.id    = 89
  s.country_name  = "Jordania"
end
Country.seed do |s|
  s.id    = 90
  s.country_name  = "Kazajistán"
end
Country.seed do |s|
  s.id    = 91
  s.country_name  = "Kenia"
end
Country.seed do |s|
  s.id    = 92
  s.country_name  = "Kirguistán"
end
Country.seed do |s|
  s.id    = 93
  s.country_name  = "Kiribati"
end
Country.seed do |s|
  s.id    = 94
  s.country_name  = "Kuwait"
end
Country.seed do |s|
  s.id    = 95
  s.country_name  = "Laos"
end
Country.seed do |s|
  s.id    = 96
  s.country_name  = "Lesoto"
end
Country.seed do |s|
  s.id    = 97
  s.country_name  = "Letonia"
end
Country.seed do |s|
  s.id    = 98
  s.country_name  = "Líbano"
end
Country.seed do |s|
  s.id    = 99
  s.country_name  = "Liberia"
end
Country.seed do |s|
  s.id    = 100
  s.country_name  = "Libia"
end
Country.seed do |s|
  s.id    = 101
  s.country_name  = "Liechtenstein"
end
Country.seed do |s|
  s.id    = 102
  s.country_name  = "Lituania"
end
Country.seed do |s|
  s.id    = 103
  s.country_name  = "Luxemburgo"
end
Country.seed do |s|
  s.id    = 104
  s.country_name  = "Madagascar"
end
Country.seed do |s|
  s.id    = 105
  s.country_name  = "Malasia"
end
Country.seed do |s|
  s.id    = 106
  s.country_name  = "Malaui"
end
Country.seed do |s|
  s.id    = 107
  s.country_name  = "Maldivas"
end
Country.seed do |s|
  s.id    = 108
  s.country_name  = "Malí"
end
Country.seed do |s|
  s.id    = 109
  s.country_name  = "Malta"
end
Country.seed do |s|
  s.id    = 110
  s.country_name  = "Marruecos"
end
Country.seed do |s|
  s.id    = 111
  s.country_name  = "Mauricio"
end
Country.seed do |s|
  s.id    = 112
  s.country_name  = "Mauritania"
end
Country.seed do |s|
  s.id    = 113
  s.country_name  = "México"
end
Country.seed do |s|
  s.id    = 114
  s.country_name  = "Micronesia"
end
Country.seed do |s|
  s.id    = 115
  s.country_name  = "Moldavia"
end
Country.seed do |s|
  s.id    = 116
  s.country_name  = "Mónaco"
end
Country.seed do |s|
  s.id    = 117
  s.country_name  = "Mongolia"
end
Country.seed do |s|
  s.id    = 118
  s.country_name  = "Montenegro"
end
Country.seed do |s|
  s.id    = 119
  s.country_name  = "Mozambique"
end
Country.seed do |s|
  s.id    = 120
  s.country_name  = "Namibia"
end
Country.seed do |s|
  s.id    = 121
  s.country_name  = "Nauru"
end
Country.seed do |s|
  s.id    = 122
  s.country_name  = "Nepal"
end
Country.seed do |s|
  s.id    = 123
  s.country_name  = "Nicaragua"
end
Country.seed do |s|
  s.id    = 124
  s.country_name  = "Níger"
end
Country.seed do |s|
  s.id    = 125
  s.country_name  = "Nigeria"
end
Country.seed do |s|
  s.id    = 126
  s.country_name  = "Noruega"
end
Country.seed do |s|
  s.id    = 127
  s.country_name  = "Nueva Zelanda"
end
Country.seed do |s|
  s.id    = 128
  s.country_name  = "Omán"
end
Country.seed do |s|
  s.id    = 129
  s.country_name  = "Países Bajos"
end
Country.seed do |s|
  s.id    = 130
  s.country_name  = "Pakistán"
end
Country.seed do |s|
  s.id    = 131
  s.country_name  = "Palaos"
end
Country.seed do |s|
  s.id    = 132
  s.country_name  = "Panamá"
end
Country.seed do |s|
  s.id    = 133
  s.country_name  = "Papúa Nueva Guinea"
end
Country.seed do |s|
  s.id    = 134
  s.country_name  = "Paraguay"
end
Country.seed do |s|
  s.id    = 135
  s.country_name  = "Perú"
end
Country.seed do |s|
  s.id    = 136
  s.country_name  = "Polonia"
end
Country.seed do |s|
  s.id    = 137
  s.country_name  = "Portugal"
end
Country.seed do |s|
  s.id    = 138
  s.country_name  = "Reino Unido"
end
Country.seed do |s|
  s.id    = 139
  s.country_name  = "República Centroafricana"
end
Country.seed do |s|
  s.id    = 140
  s.country_name  = "República Checa"
end
Country.seed do |s|
  s.id    = 141
  s.country_name  = "República de Macedonia"
end
Country.seed do |s|
  s.id    = 142
  s.country_name  = "República del Congo"
end
Country.seed do |s|
  s.id    = 143
  s.country_name  = "República Democrática del Congo"
end
Country.seed do |s|
  s.id    = 144
  s.country_name  = "República Dominicana"
end
Country.seed do |s|
  s.id    = 145
  s.country_name  = "República Sudafricana"
end
Country.seed do |s|
  s.id    = 146
  s.country_name  = "Ruanda"
end
Country.seed do |s|
  s.id    = 147
  s.country_name  = "Rumanía"
end
Country.seed do |s|
  s.id    = 148
  s.country_name  = "Rusia"
end
Country.seed do |s|
  s.id    = 149
  s.country_name  = "Samoa"
end
Country.seed do |s|
  s.id    = 150
  s.country_name  = "San Cristóbal y Nieves"
end
Country.seed do |s|
  s.id    = 151
  s.country_name  = "San Marino"
end
Country.seed do |s|
  s.id    = 152
  s.country_name  = "San Vicente y las Granadinas"
end
Country.seed do |s|
  s.id    = 153
  s.country_name  = "Santa Lucía"
end
Country.seed do |s|
  s.id    = 154
  s.country_name  = "Santo Tomé y Príncipe"
end
Country.seed do |s|
  s.id    = 155
  s.country_name  = "Senegal"
end
Country.seed do |s|
  s.id    = 156
  s.country_name  = "Serbia"
end
Country.seed do |s|
  s.id    = 157
  s.country_name  = "Seychelles"
end
Country.seed do |s|
  s.id    = 158
  s.country_name  = "Sierra Leona"
end
Country.seed do |s|
  s.id    = 159
  s.country_name  = "Singapur"
end
Country.seed do |s|
  s.id    = 160
  s.country_name  = "Siria"
end
Country.seed do |s|
  s.id    = 161
  s.country_name  = "Somalia"
end
Country.seed do |s|
  s.id    = 162
  s.country_name  = "Sri Lanka"
end
Country.seed do |s|
  s.id    = 163
  s.country_name  = "Suazilandia"
end
Country.seed do |s|
  s.id    = 164
  s.country_name  = "Sudán"
end
Country.seed do |s|
  s.id    = 165
  s.country_name  = "Sudán del Sur"
end
Country.seed do |s|
  s.id    = 166
  s.country_name  = "Suecia"
end
Country.seed do |s|
  s.id    = 167
  s.country_name  = "Suiza"
end
Country.seed do |s|
  s.id    = 168
  s.country_name  = "Surinam"
end
Country.seed do |s|
  s.id    = 169
  s.country_name  = "Tailandia"
end
Country.seed do |s|
  s.id    = 170
  s.country_name  = "Tanzania"
end
Country.seed do |s|
  s.id    = 171
  s.country_name  = "Tayikistán"
end
Country.seed do |s|
  s.id    = 172
  s.country_name  = "Timor Oriental"
end
Country.seed do |s|
  s.id    = 173
  s.country_name  = "Togo"
end
Country.seed do |s|
  s.id    = 174
  s.country_name  = "Tonga"
end
Country.seed do |s|
  s.id    = 175
  s.country_name  = "Trinidad y Tobago"
end
Country.seed do |s|
  s.id    = 176
  s.country_name  = "Túnez"
end
Country.seed do |s|
  s.id    = 177
  s.country_name  = "Turkmenistán"
end
Country.seed do |s|
  s.id    = 178
  s.country_name  = "Turquía"
end
Country.seed do |s|
  s.id    = 179
  s.country_name  = "Tuvalu"
end
Country.seed do |s|
  s.id    = 180
  s.country_name  = "Ucrania"
end
Country.seed do |s|
  s.id    = 181
  s.country_name  = "Uganda"
end
Country.seed do |s|
  s.id    = 182
  s.country_name  = "Uruguay"
end
Country.seed do |s|
  s.id    = 183
  s.country_name  = "Uzbekistán"
end
Country.seed do |s|
  s.id    = 184
  s.country_name  = "Vanuatu"
end
Country.seed do |s|
  s.id    = 185
  s.country_name  = "Venezuela"
end
Country.seed do |s|
  s.id    = 186
  s.country_name  = "Vietnam"
end
Country.seed do |s|
  s.id    = 187
  s.country_name  = "Yemen"
end
Country.seed do |s|
  s.id    = 188
  s.country_name  = "Yibuti"
end
Country.seed do |s|
  s.id    = 189
  s.country_name  = "Zambia"
end
Country.seed do |s|
  s.id    = 190
  s.country_name  = "Zimbabue"
end
Country.seed do |s|
  s.id    = 191
  s.country_name  = "Afganistán"
end
Country.seed do |s|
  s.id    = 192
  s.country_name  = "Albania"
end
Country.seed do |s|
  s.id    = 193
  s.country_name  = "Alemania"
end
Country.seed do |s|
  s.id    = 194
  s.country_name  = "Andorra"
end
Country.seed do |s|
  s.id    = 195
  s.country_name  = "Puerto Rico"
end
