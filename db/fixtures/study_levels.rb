StudyLevel.seed do |s|
  s.id    = 1
  s.study_level_name  =  'Nivel 1'
end
StudyLevel.seed do |s|
  s.id    = 2
  s.study_level_name  =  'Primaria'
end
StudyLevel.seed do |s|
  s.id    = 3
  s.study_level_name  =  'Bachillerato'
end
StudyLevel.seed do |s|
  s.id    = 4
  s.study_level_name  =  'Técnico laboral'
end
StudyLevel.seed do |s|
  s.id    = 5
  s.study_level_name  =  'Técnico profesional'
end
StudyLevel.seed do |s|
  s.id    = 6
  s.study_level_name  =  'Profesional'
end
StudyLevel.seed do |s|
  s.id    = 7
  s.study_level_name  =  'Especialización'
end
StudyLevel.seed do |s|
  s.id    = 8
  s.study_level_name  =  'Maestría'
end
StudyLevel.seed do |s|
  s.id    = 9
  s.study_level_name  =  'Doctorado'
end
StudyLevel.seed do |s|
  s.id    = 10
  s.study_level_name  =  'Ninguno'
end












