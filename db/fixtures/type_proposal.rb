TypeProposal.seed do |s|
  s.id    = 1
  s.type_proposal_name  =  'Cantante/Solista'
end
TypeProposal.seed do |s|
  s.id    = 2
  s.type_proposal_name  =  'Compositor'
end
TypeProposal.seed do |s|
  s.id    = 3
  s.type_proposal_name  =  'Banda/Orquesta'
end
TypeProposal.seed do |s|
  s.id    = 4
  s.type_proposal_name  =  'Músico/Instrumentista'
end
TypeProposal.seed do |s|
  s.id    = 5
  s.type_proposal_name  =  'DJ/Productor'
end
TypeProposal.seed do |s|
  s.id    = 6
  s.type_proposal_name  =  'Otro'
end
