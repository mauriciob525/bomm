class CreateCompanyRepresentatives < ActiveRecord::Migration[5.0]
  def change
    create_table :company_representatives do |t|
      t.string :name_comp
      t.string :nit
      t.string :cod_verification
      t.references :project_legal_represent, foreign_key: true

      t.timestamps
    end
  end
end
