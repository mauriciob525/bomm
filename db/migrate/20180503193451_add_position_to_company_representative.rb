class AddPositionToCompanyRepresentative < ActiveRecord::Migration[5.0]
  def change
    add_reference :company_representatives, :position, foreign_key: true
  end
end
