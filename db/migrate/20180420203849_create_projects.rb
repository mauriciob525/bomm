class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.references :type_proposal, foreign_key: true
      t.references :type_gender, foreign_key: true
      t.text :review_esp
      t.text :review_eng
      t.string :manager
      t.float :cost_live_show
      t.boolean :ccb
      t.boolean :showcases
      t.timestamps
    end
  end
end
