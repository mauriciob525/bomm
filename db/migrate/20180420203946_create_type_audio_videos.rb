class CreateTypeAudioVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :type_audio_videos do |t|
      t.string :type_audio_name

      t.timestamps
    end
  end
end
