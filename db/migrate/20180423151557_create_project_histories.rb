class CreateProjectHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :project_histories do |t|
      t.references :project, foreign_key: true
      t.references :status, foreign_key: true
      t.datetime :date_save

      t.timestamps
    end
  end
end
