class CreateProjectSocialNetworks < ActiveRecord::Migration[5.0]
  def change
    create_table :project_social_networks do |t|
      t.string :link
      t.references :project, foreign_key: true
      t.references :type_social_network, foreign_key: true

      t.timestamps
    end
  end
end
