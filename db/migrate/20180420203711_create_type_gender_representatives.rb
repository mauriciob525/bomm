class CreateTypeGenderRepresentatives < ActiveRecord::Migration[5.0]
  def change
    create_table :type_gender_representatives do |t|
      t.string :type_gender_name

      t.timestamps
    end
  end
end
