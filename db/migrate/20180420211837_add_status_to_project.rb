class AddStatusToProject < ActiveRecord::Migration[5.0]
  def change
    add_reference :projects, :status, foreign_key: true
  end
end
