class CreateTypeProposals < ActiveRecord::Migration[5.0]
  def change
    create_table :type_proposals do |t|
      t.string :type_proposal_name

      t.timestamps
    end
  end
end
