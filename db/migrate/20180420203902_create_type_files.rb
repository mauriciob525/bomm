class CreateTypeFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :type_files do |t|
      t.string :type_file_name

      t.timestamps
    end
  end
end
