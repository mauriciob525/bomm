class CreateProjectFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :project_files do |t|
      t.string :file
      t.references :project, foreign_key: true
      t.references :type_file, foreign_key: true

      t.timestamps
    end
  end
end
