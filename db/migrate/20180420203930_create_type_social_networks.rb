class CreateTypeSocialNetworks < ActiveRecord::Migration[5.0]
  def change
    create_table :type_social_networks do |t|
      t.string :type_social_network_name

      t.timestamps
    end
  end
end
