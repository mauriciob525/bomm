class AddDataToProjectLegalRepresent < ActiveRecord::Migration[5.0]
  def change
    add_column :project_legal_represents, :second_name, :string
    add_column :project_legal_represents, :second_surname, :string
    add_column :project_legal_represents, :email_opcional, :string
  end
end
