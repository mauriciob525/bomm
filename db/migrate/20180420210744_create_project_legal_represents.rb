class CreateProjectLegalRepresents < ActiveRecord::Migration[5.0]
  def change
    create_table :project_legal_represents do |t|
      t.string :name
      t.string :last_name
      t.references :type_gender_representative, foreign_key: true
      t.references :type_document, foreign_key: true
      t.string :document
      t.date :birth_date
      t.references :country, foreign_key: true
      t.references :city, foreign_key: true
      t.string :other_city
      t.string :address
      t.string :phone
      t.string :cell_phone
      t.string :email
      t.references :study_level, foreign_key: true
      t.boolean :represents_company
      t.references :project, foreign_key: true
      t.boolean :notification_text
      t.boolean :notification_email
      t.timestamps
    end
  end
end
