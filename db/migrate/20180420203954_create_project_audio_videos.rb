class CreateProjectAudioVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :project_audio_videos do |t|
      t.text :link
      t.references :project, foreign_key: true
      t.references :type_audio_video, foreign_key: true

      t.timestamps
    end
  end
end
