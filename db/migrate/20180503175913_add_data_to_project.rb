class AddDataToProject < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :terms, :boolean
    add_column :projects, :data_policy, :boolean
  end
end
