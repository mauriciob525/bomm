class CreateStudyLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :study_levels do |t|
      t.string :study_level_name

      t.timestamps
    end
  end
end
