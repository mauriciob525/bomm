class CreateTypeGenders < ActiveRecord::Migration[5.0]
  def change
    create_table :type_genders do |t|
      t.string :type_gender_name

      t.timestamps
    end
  end
end
