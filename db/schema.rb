# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180503193451) do

  create_table "cities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "city_name"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["department_id"], name: "index_cities_on_department_id", using: :btree
  end

  create_table "company_representatives", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name_comp"
    t.string   "nit"
    t.string   "cod_verification"
    t.string   "position"
    t.integer  "project_legal_represent_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "position_id"
    t.index ["position_id"], name: "index_company_representatives_on_position_id", using: :btree
    t.index ["project_legal_represent_id"], name: "index_company_representatives_on_project_legal_represent_id", using: :btree
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "country_name"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "departments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "department_name"
    t.integer  "country_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["country_id"], name: "index_departments_on_country_id", using: :btree
  end

  create_table "positions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "project_audio_videos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "link"
    t.integer  "project_id"
    t.integer  "type_audio_video_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["project_id"], name: "index_project_audio_videos_on_project_id", using: :btree
    t.index ["type_audio_video_id"], name: "index_project_audio_videos_on_type_audio_video_id", using: :btree
  end

  create_table "project_files", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "file"
    t.integer  "project_id"
    t.integer  "type_file_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["project_id"], name: "index_project_files_on_project_id", using: :btree
    t.index ["type_file_id"], name: "index_project_files_on_type_file_id", using: :btree
  end

  create_table "project_histories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "project_id"
    t.integer  "status_id"
    t.datetime "date_save"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_project_histories_on_project_id", using: :btree
    t.index ["status_id"], name: "index_project_histories_on_status_id", using: :btree
  end

  create_table "project_legal_represents", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "last_name"
    t.integer  "type_gender_representative_id"
    t.integer  "type_document_id"
    t.string   "document"
    t.date     "birth_date"
    t.integer  "country_id"
    t.integer  "city_id"
    t.string   "other_city"
    t.string   "address"
    t.string   "phone"
    t.string   "cell_phone"
    t.string   "email"
    t.integer  "study_level_id"
    t.boolean  "represents_company"
    t.integer  "project_id"
    t.boolean  "notification_text"
    t.boolean  "notification_email"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "second_name"
    t.string   "second_surname"
    t.string   "email_opcional"
    t.index ["city_id"], name: "index_project_legal_represents_on_city_id", using: :btree
    t.index ["country_id"], name: "index_project_legal_represents_on_country_id", using: :btree
    t.index ["project_id"], name: "index_project_legal_represents_on_project_id", using: :btree
    t.index ["study_level_id"], name: "index_project_legal_represents_on_study_level_id", using: :btree
    t.index ["type_document_id"], name: "index_project_legal_represents_on_type_document_id", using: :btree
    t.index ["type_gender_representative_id"], name: "index_project_legal_represents_on_type_gender_representative_id", using: :btree
  end

  create_table "project_social_networks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "link"
    t.integer  "project_id"
    t.integer  "type_social_network_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["project_id"], name: "index_project_social_networks_on_project_id", using: :btree
    t.index ["type_social_network_id"], name: "index_project_social_networks_on_type_social_network_id", using: :btree
  end

  create_table "projects", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "type_proposal_id"
    t.integer  "type_gender_id"
    t.text     "review_esp",       limit: 65535
    t.text     "review_eng",       limit: 65535
    t.string   "manager"
    t.float    "cost_live_show",   limit: 24
    t.boolean  "ccb"
    t.boolean  "showcases"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "status_id"
    t.integer  "user_id"
    t.boolean  "terms"
    t.boolean  "data_policy"
    t.index ["status_id"], name: "index_projects_on_status_id", using: :btree
    t.index ["type_gender_id"], name: "index_projects_on_type_gender_id", using: :btree
    t.index ["type_proposal_id"], name: "index_projects_on_type_proposal_id", using: :btree
    t.index ["user_id"], name: "index_projects_on_user_id", using: :btree
  end

  create_table "statuses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "status_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "study_levels", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "study_level_name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "type_audio_videos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_audio_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "type_documents", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_document_name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "type_files", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_file_name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "type_gender_representatives", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_gender_name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "type_genders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_gender_name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "type_proposals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_proposal_name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "type_social_networks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_social_network_name"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "provider",                             default: "email", null: false
    t.string   "uid",                                  default: "",      null: false
    t.string   "encrypted_password",                   default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean  "allow_password_change",                default: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "email"
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  add_foreign_key "cities", "departments"
  add_foreign_key "company_representatives", "positions"
  add_foreign_key "company_representatives", "project_legal_represents"
  add_foreign_key "departments", "countries"
  add_foreign_key "project_audio_videos", "projects"
  add_foreign_key "project_audio_videos", "type_audio_videos"
  add_foreign_key "project_files", "projects"
  add_foreign_key "project_files", "type_files"
  add_foreign_key "project_histories", "projects"
  add_foreign_key "project_histories", "statuses"
  add_foreign_key "project_legal_represents", "cities"
  add_foreign_key "project_legal_represents", "countries"
  add_foreign_key "project_legal_represents", "projects"
  add_foreign_key "project_legal_represents", "study_levels"
  add_foreign_key "project_legal_represents", "type_documents"
  add_foreign_key "project_legal_represents", "type_gender_representatives"
  add_foreign_key "project_social_networks", "projects"
  add_foreign_key "project_social_networks", "type_social_networks"
  add_foreign_key "projects", "statuses"
  add_foreign_key "projects", "type_genders"
  add_foreign_key "projects", "type_proposals"
  add_foreign_key "projects", "users"
end
