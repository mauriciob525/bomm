class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'


	def proyecto_publicado(email)
		mail_envio=email

		mail(:to => email, 
		    :subject => 'proyecto publicado', 
		    :template_path => 'user_mailer',
		    :template_name => '/devise/mailer/proyecto_publicado')
 	end

 end