class Project < ApplicationRecord
  belongs_to :type_proposal
  belongs_to :type_gender
  attr_accessor :is_public_project

  validates :type_proposal_id, :presence => { message: "Tipo de propuesta nulo" } , if: :public_project
  validates :type_gender_id, :presence => { message: "Tipo de Genero nulo" } , if: :public_project
  validates :review_esp, :presence => { message: "Reseña en español nulo" } , if: :public_project
  validates :review_eng, :presence => { message: "Reseña en ingles nulo" } , if: :public_project
  validates :manager, :presence => { message: "Reseña en ingles nulo" } , if: :public_project
  validates :cost_live_show, :presence => { message: "Costo de show en vivo nulo" } , if: :public_project


  def public_project
    return is_public_project
  end

  def self.getProjectByUser(user_id)
    Project.where(user_id:user_id)
    .joins('inner join users on users.id=projects.user_id').select('projects.*,users.name')
    
  end

end
