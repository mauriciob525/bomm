class ProjectSocialNetwork < ApplicationRecord
  belongs_to :project
  belongs_to :type_social_network
end
