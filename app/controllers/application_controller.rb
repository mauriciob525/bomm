class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken
   respond_to :json
   #protect_from_forgery with: :exception
   protect_from_forgery with: :null_session
   after_action :set_csrf_cookie



	def set_csrf_cookie
	  cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
	end

	def login
            puts current_user
	end
	protected

    # In Rails 4.2 and above
	def verified_request?
	    super || valid_authenticity_token?(session, request.headers['X-XSRF-TOKEN'])
	end	
end
