class UserController < ApplicationController
      before_action :authenticate_user!
      before_action :login
       def test
            @resource = resource_class.confirm_by_token(params[:confirmation_token])

            respond_to do |format|
                  format.json { render:json => { 
                                                error: false,
                                                msg:'project save',
                                          }
                                       }
                end
      end
	def create_project
		#user_id=1

            puts params[:name]
            puts ">>>>>>>>>>>>>>>>>>>>>>>"
            user=current_user
            user.name=params[:name]
            user.save
            project=Project.find_by(user_id:user.id)

            if params[:status_id].to_i==2
                  project=Project.find_by(user_id:user.id)
                  status_id=project.status_id
                  project.status_id=2
                  project.terms=params[:terminos_condiciones]
                  project.data_policy=params[:tratamiento_datos]
                  project.save

                  if project.status_id.to_i==2
                        h=ProjectHistory.new
                        h.project_id=project.id
                        h.date_save=project.created_at
                        h.status_id=status_id
                        h.save
                  end

                  ApplicationMailer.proyecto_publicado(user.email).deliver_later
                  
                  respond_to do |format|
                  format.json { render :json => {
                                                error: false,
                                                msg: 'Proyecto publicado'
                                          }
                                       }
                end
                return false;
            end
		
		if project.blank?
			project=Project.new
			project.user_id=user.id
		end
		project.type_proposal_id=params[:type_proposal_id]
		project.type_gender_id=params[:type_gender_id]
		project.review_esp=params[:review_esp]
		project.review_eng=params[:review_eng]
		project.manager=params[:manager]
		project.cost_live_show=params[:cost_live_show]
		project.ccb=params[:ccb]
            status_id=project.status_id
		project.status_id=params[:status_id]
		if project.status_id.to_i==2
			project.is_public_project=true
		end
		project.showcases=params[:showcases]
		project.save

		project.valid?
            if project.errors.any?
          	respond_to do |format|
		      format.json { render :json => {
			      				error: true,
			      				msg: project.errors.messages.values
			      			}
		      			   }
		    end
            else
                  #history
                  
                  
                  #end history
      		#archivos imagen
      			if !params[:imagen].blank?
      				file=ProjectFile.find_by(type_file_id:1)
      				if file.blank?
      					file=ProjectFile.new
      					file.type_file_id=1
      				end
      				file.project_id = project.id
      				file.file=params[:imagen]
      				file.save
      			end
      		#end archivos imagen

      		#archivos imagen
      			if !params[:pdf].blank?
      				file=ProjectFile.find_by(type_file_id:2)
      				if file.blank?
      					file=ProjectFile.new
      					file.type_file_id=2
      				end
      				file.project_id = project.id
      				file.file=params[:pdf]
      				file.save
      			end
      		#end archivos imagen

      		#Redes sociales
      		ProjectSocialNetwork.where(project_id:project.id).destroy_all
      		if !params[:social].blank?
		        params[:social].each do |s|
		          project_social = ProjectSocialNetwork.new
		          project_social.link = s['link']
		          project_social.type_social_network_id = s['type_social_network_id']
		          project_social.project_id = project.id
		          project_social.save
		        end
		    end
		    #end Redes sociales

		    #audio y video
      		ProjectAudioVideo.where(project_id:project.id).destroy_all
      		if !params[:audio_video].blank?
		        params[:audio_video].each do |s|
		          project_social = ProjectAudioVideo.new
		          project_social.link = s['link']
		          project_social.type_audio_video_id = s['type_audio_video_id']
		          project_social.project_id = project.id
		          project_social.save
		        end
		    end
		    #end audio y video


		    #Representante legal
      		legal=ProjectLegalRepresent.find_by(project_id:project.id)
      		if legal.blank?
      			legal=ProjectLegalRepresent.new
      		end
      		legal.name=params[:first_name]
                  legal.second_name=params[:second_name]
      		legal.last_name=params[:last_name]
                  legal.second_surname=params[:second_surname]
      		legal.type_gender_representative_id=params[:type_gender_representative_id]
      		legal.type_document_id=params[:type_document_id]
      		legal.document=params[:document]
      		legal.birth_date=params[:birth_date]

      		legal.country_id=params[:country_id]
      		if params[:country_id]=='38'
      			legal.city_id=params[:city_id]
      			legal.other_city=nil
      		else
      			legal.city_id=nil
      			legal.other_city=params[:other_city]
      		end
      		
      		legal.address=params[:address]
      		legal.phone=params[:phone]
      		legal.cell_phone=params[:cell_phone]
      		legal.email=params[:email]
                  legal.email_opcional=params[:email_opcional]
      		legal.study_level_id=params[:study_level_id]
      		legal.represents_company=params[:represents_company]
      		legal.project_id=project.id
      		legal.notification_text=params[:notification_text]
      		legal.notification_email=params[:notification_email]
      		legal.save
      	
      		if params[:represents_company]==true
      			compa_repre=CompanyRepresentative.find_by(project_legal_represent_id:legal.id)
      			if compa_repre.blank?
      				compa_repre=CompanyRepresentative.new
      			end
      			compa_repre.project_legal_represent_id=legal.id
      			compa_repre.name_comp=params[:name_comp]
      			compa_repre.nit=params[:nit]
      			compa_repre.cod_verification=params[:cod_verification]
      			compa_repre.position_id=params[:position]
      			compa_repre.save
      		end
		    #end Representante legal
		    respond_to do |format|
		      format.json { render:json => { 
		      					error: false,
		      					msg:'project save',
		      				}
		      			   }
		    end
            end
	end

      def  getInfo

            respond_to do |format|
                  format.json { render :json => {
                                          type_porposal: TypeProposal.all,    
                                          type_gender: TypeGender.all,
                                          type_social_network: TypeSocialNetwork.all,
                                          type_audio:TypeAudioVideo.where('id in (1,2,3,4,5,6)'),
                                          type_video:TypeAudioVideo.where('id in (7,8)'),
                                          type_gender_representative: TypeGenderRepresentative.all,
                                          type_document: TypeDocument.all,
                                          country: Country.all,
                                          departament: Department.all,
                                          city: City.all,
                                          study_level: StudyLevel.all,
                                          position: Position.all,
                                    }
                  }
            end  
      end
      def getProject
            #user_id=1
            user=current_user
            project=Project.getProjectByUser(user.id).take
            if project.blank?
                  respond_to do |format|
                        format.json { render :json => {
                                                error: true,
                                                msg: 'No tiene proyectos creados'
                                          }
                                       }
                  end  
            else
                  imagen=ProjectFile.where(project_id:project.id).where(type_file_id:1).take
                  pdf=ProjectFile.where(project_id:project.id).where(type_file_id:2).take     
                  social_network=ProjectSocialNetwork.where(project_id:project.id)     
                  audio=ProjectAudioVideo.where(project_id:project.id).where('type_audio_video_id in (1,2,3,4,5,6)')
                  video=ProjectAudioVideo.where(project_id:project.id).where('type_audio_video_id in (7,8)')
                  legal_representative=ProjectLegalRepresent.getLegalByProject(project.id).take
                  respond_to do |format|
                        format.json { render :json => {
                                                error: false,
                                                project:project,
                                                imagen:imagen,
                                                pdf:pdf,
                                                social_network:social_network,
                                                audio:audio,
                                                video:video,
                                                legal_representative:legal_representative
                                          }
                                       }
                  end 
            end
            
      end

      def active_project
            #user_id=1
            user=User.find_by(tokens:params[:token])
            project=Project.find_by(user_id:user.id)
            if project.blank?
                  respond_to do |format|
                        format.json { render :json => {
                                                error: true,
                                                msg: 'No tiene proyectos creados'
                                          }
                                       }
                  end  
            else
                  #hisotory
                  h=ProjectHistory.new
                  h.project_id=project.id
                  h.date_save=project.updated_at
                  h.status_id=project.status_id
                  project.status_id=3
                  project.save
                  #end hisotory
                  respond_to do |format|
                        format.json { render :json => {
                                                error: false,
                                                msg: 'projecto activo'
                                          }
                                       }
                  end  
            end
      end
end
