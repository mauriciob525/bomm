class IndexController < ApplicationController
 
  def index
  	respond_to do |format|
      format.json { render json: User.all }
    end
  end

  def reset_password

  	user=User.find_by(reset_password_token:params[:resetPasswordToken])
  	if !user.blank?
  		user.password=params[:password]
  		user.password_confirmation=params[:passwordConfirmation]
  		user.save
  	end

  	respond_to do |format|
                  format.json { render :json => {
                                                error: false,
                                                msg: ''
                                          }
                                       }
                end
  	
  end


end
