Rails.application.routes.draw do
  get 'index/index'
  devise_scope :user do
    get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
    post "/sign_up" => "devise/registrations#create", as: "new_user_registration",:defaults => { :format => 'json' } # custom path to sign_up/registration
    post '/create_project', to: 'user#create_project'  , :defaults => { :format => 'json' }
    get '/getInfo', to: 'user#getInfo'  , :defaults => { :format => 'json' }
    post '/getProject', to: 'user#getProject'  , :defaults => { :format => 'json' }
    get '/user/test', to: 'user#test'  , :defaults => { :format => 'json' }
    
    
  end

  post '/index/reset_password', to: 'index#reset_password'  , :defaults => { :format => 'json' }

  mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :index, only: [:index, :create, :destroy], defaults: {format: :json}
  root :to => "index#index"
  
end
