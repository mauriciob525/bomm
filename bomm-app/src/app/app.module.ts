import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';

import { PdfViewerModule } from 'ng2-pdf-viewer';
import { EmbedVideo } from 'ngx-embed-video';

import { Angular2TokenService } from 'angular2-token';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { RegistroComponent } from './registro/registro.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { FormularioComponent } from './formulario/formulario.component';

import { AuthService } from './services/auth.service';
import { RegistroService } from './services/registro.service';
import { AuthGuard } from './guards/auth.guard';
import { SanitizeHtmlPipe } from './pipes/sanitize-html.pipe';
import { DragNdropDirective } from './components/file-uploader-s3/drag-ndrop.directive';
import { FileUploaderS3Component } from './components/file-uploader-s3/file-uploader-s3.component';
import { PreviewComponent } from './preview/preview.component';
import { TerminosComponent } from './terminos/terminos.component'
import { DialogComponent } from './components/dialog/dialog.component';
import { GraciasComponent } from './gracias/gracias.component';
import { RecuperarPasswordComponent } from './recuperar-password/recuperar-password.component';
import { SpotifyComponent } from './components/sound/spotify/spotify.component';
import { SoundComponent } from './components/sound/sound.component';
import { SanitizeUrlPipe } from './pipes/sanitize-url.pipe';
import { SoundcloudComponent } from './components/sound/soundcloud/soundcloud.component';
import { ReverbnationComponent } from './components/sound/reverbnation/reverbnation.component';
import { DeezerComponent } from './components/sound/deezer/deezer.component';
import { BandcampComponent } from './components/sound/bandcamp/bandcamp.component';
import { MixcloudComponent } from './components/sound/mixcloud/mixcloud.component';
import { YoutubeComponent } from './components/sound/youtube/youtube.component';
import { VimeoComponent } from './components/sound/vimeo/vimeo.component';
import { ConfirmarContrasenaComponent } from './confirmar-contrasena/confirmar-contrasena.component'

import { ImagesizeDirective } from './components/imagesize.directive';


@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    FormularioComponent,
    SanitizeHtmlPipe,
    FileUploaderS3Component,
    DragNdropDirective,
    PreviewComponent,
    TerminosComponent,
    DialogComponent,
    GraciasComponent,
    RecuperarPasswordComponent,
    SpotifyComponent,
    SoundComponent,
    SanitizeUrlPipe,
    SoundcloudComponent,
    ReverbnationComponent,
    DeezerComponent,
    BandcampComponent,
    MixcloudComponent,
    YoutubeComponent,
    VimeoComponent,
    ConfirmarContrasenaComponent,
    ImagesizeDirective
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    PdfViewerModule,
    EmbedVideo.forRoot()
  ],
   entryComponents: [
    DialogComponent,
  ],
  providers: [Angular2TokenService, AuthService, RegistroService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
