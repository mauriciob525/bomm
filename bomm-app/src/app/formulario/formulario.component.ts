import { Component, OnInit } from '@angular/core';
import { RegistroService } from '../services/registro.service';
import { Proyecto } from './proyecto';
import { Router } from '@angular/router';
import { AppSettings } from '../app.settings';




@Component({
	selector: 'app-formulario',
	templateUrl: './formulario.component.html',
	styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

	patterns = {
		website: "",
		alpha:/[a-zA-ZÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÜüÑñ]+/,
		alpha_numeric:/[a-zA-Z0-9ÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÜüÑñ#_()@:,.\-\–_]+/,
		alpha_space: /[a-zA-Z0-9ÁáÀàÉéÈèÍíÌìÓóÒòÚúÙùÜüÑñ#_()@:,.\-\–_]+/,
		number: /^[-+]?\d*(?:[\.\,]\d+)?$/,
		phone: /^[-+]?\d*(?:[\.\,]\d+)?$/,
		email: /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/,
		currency: /^[-+]?\d*(?:[\.\,]\d+)?$/,
	};

	cities = [];
	fill_cities = [];
	department = [];
	contries = [];

	loader:boolean = false;

	generos = [];
	tipos = [];
	redes = [];
	niveles_estudio = [];
	cargo_empresa = [];
	tipos_docs = [];
	tipos_video = [];
	tipos_audio = [];

	si_no = [{label:'Si', value:true}, {label:'No', value:false}];

	sexo = [ {id:1, type_gender_name:'Femenino'}, {id:2, type_gender_name:'Masculino'}];

	model:any = {};

	constructor(private regService: RegistroService, private router:Router) { 

		this.model = new Proyecto();

		this.model['audio_video'] = [
		{type_audio_video_id:"", link:"", valid:true},
		{type_audio_video_id:"", link:"", valid:true},
		{type_audio_video_id:"", link:"", valid:true},
		{type_audio_video_id:"", link:"", valid:true},
		{type_audio_video_id:"", link:"", valid:true},
		{type_audio_video_id:"", link:"", valid:true}
		];

		this.model['social'] = [
		{type_social_network_id:"1", link:""},
		{type_social_network_id:"", link:""},
		{type_social_network_id:"", link:""}
		];


		this.regService.getInfo().subscribe( data => {
			this.contries = data.country;
			this.department = data.departament;
			this.cities = data.city;
			this.niveles_estudio = data.study_level;
			this.tipos_audio = data.type_audio;
			this.tipos_video = data.type_video;
			this.tipos_docs = data.type_document;
			this.generos = data.type_gender;
			this.sexo = data.type_gender_representative;
			this.tipos = data.type_porposal;
			this.redes = data.type_social_network.filter( red => red.id != 1 );

			this.regService.getProyect().subscribe( data => {

				if(!data.error){
					this.model['audio_video'] = [];

					for(let field in data.project){
						console.log(field, data.project[field])
						this.model[field] = data.project[field];
					}

					for(let field in data.legal_representative){
						this.model[field] = data.legal_representative[field];
					}

					for(let i = 0; i < 3; i++){
						if(data.audio[i]){
							this.model['audio_video'].push({
								type_audio_video_id: data.audio[i]['type_audio_video_id'],
								link: data.audio[i]['link']
							});	
						}else{
							this.model['audio_video'].push({
								type_audio_video_id: "",
								link: ""
							});	
						}
					}

					for(let i = 0; i < 3; i++){
						if(data.video[i]){
							this.model['audio_video'].push({
								type_audio_video_id: data.video[i]['type_audio_video_id'],
								link: data.video[i]['link']
							});	
						}else{
							this.model['audio_video'].push({
								type_audio_video_id: "",
								link: ""
							});	
						}
					}

					if(data.social_network.length > 0){
						this.model['social'] = [];
						let website = data.social_network.filter( f => f.type_social_network_id == 1)[0];
						
						this.model['social'][0] = website ? {
							type_social_network_id: website['type_social_network_id'],
							link: website['link']
						}: {type_social_network_id:"1", link:""};

						

						for(let field of data.social_network.filter( f => f.type_social_network_id != 1)){
							this.model['social'].push({
								type_social_network_id: field['type_social_network_id'],
								link: field['link']
							});
						}
					}
					

					if(data.imagen){
						this.model['imagen'] = data.imagen.file;	
					}

					if(data.pdf){
						this.model['pdf'] = data.pdf.file;	
					}
				}
				
			})

		});
	}

	ngOnInit() {
		console.log(AppSettings.NAME.length);
		if(AppSettings.NAME.length > 0){
			console.log("Entro: ",AppSettings.NAME.length);
			this.model['name'] = AppSettings.NAME;
			AppSettings.NAME = "";
			this.saveData();
		}
	}

	saveData(){
		this.model['status_id'] = 1;
		this.regService.createProyect(this.model)
		.subscribe( data => {
			console.log('post ok', data);
		});
	}

	onSubmit(){
		if(!this.verifyLinks()){
			this.model['status_id'] = 2;
			this.regService.createProyect(this.model)
			.subscribe( data => {
				this.router.navigate(['/confirmar']);
			});
		}
		
		
	}



	fillCity(id_departament){
		this.fill_cities = this.cities.filter( city => city.department_id == parseInt(id_departament));
	}

	onLoadFile(evt){
		this.loader = evt;
		
	}

	submitImage(evt){
		this.model['imagen'] = evt.url;
		this.saveData();
		this.loader = false;
	}

	submitFile(evt){
		this.model['pdf'] = evt.url;
		this.saveData();
		this.loader = false;

	}

	saveModel(isFail){
		if(!isFail){
			this.saveData();
		}
	}

	saveMultimediaModel(isFail, value){

		if(!isFail){
			let verify:boolean = this.verifyLinks(value.link);

			if(verify){
				this.saveData();	
			}

			value.valid = verify;
			
		}

		
	}	

	savecheckModel(){
		this.saveData();
	}

	verifyLinks(link = ""){
		let linkVerify:boolean = false;

		

		if(link.length > 0){
			let fill = this.model['audio_video'].filter( audio => audio.link == link);
		
			if(fill.length > 1){
				linkVerify = true;
			}
		}else{
			let _audio = "";

			for(let audio of this.model['audio_video']){
				

				if(_audio == audio.link){
					linkVerify = true;
				}else{
					_audio = audio.link;	
				}
			}
		}
		return linkVerify;


	}

}