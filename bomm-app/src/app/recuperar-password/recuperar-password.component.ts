import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-recuperar-password',
	templateUrl: './recuperar-password.component.html',
	styleUrls: ['./recuperar-password.component.scss']
})
export class RecuperarPasswordComponent implements OnInit {


	loader:boolean;

	resetPasswordUser = {
		email: ''
	};

	errorForm = null;

	constructor(private authService: AuthService, private router:Router) { }

	ngOnInit() {
	}

	onSignInSubmit(){
		this.loader = true;
		this.authService.forgotPassword(this.resetPasswordUser)
		.subscribe( 
			res => {
				if(res.status == 200){
					this.router.navigate(['registro']);
					this.loader = false;
				}
			}, 
			err => {
				console.log(err);
				this.loader = false;
				this.errorForm = "Hubo un problema al recuperar la contraseña. Inténtalo de nuevo.";
			}

			)
	}

}
