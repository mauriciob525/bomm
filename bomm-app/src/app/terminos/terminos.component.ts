import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { DialogComponent } from '../components/dialog/dialog.component';
import { AppSettings } from '../app.settings';
import { RegistroService } from '../services/registro.service';
import { Router } from '@angular/router';


@Component({
	selector: 'app-terminos',
	templateUrl: './terminos.component.html',
	styleUrls: ['./terminos.component.scss']
})
export class TerminosComponent implements OnInit {


	model:any = {};
	//terminos_url = AppSettings.URL_BACKEND + 'Reglamento_BOmm_2018_v1.pdf';
	terminos_url ='assets/images/Reglamento_BOmm_2018_v1.pdf';

	constructor(public dialog: MatDialog, private regService: RegistroService, private router:Router) { }

	openDialog(): void {

		let dialogRef = this.dialog.open(DialogComponent, {
			width: '80%'
		});

	}

	ngOnInit() {
	}

	onSubmit(){
		this.model['status_id'] = 2;

		this.regService.createProyect(this.model)
			.subscribe( data => {
				this.router.navigate(['/gracias']);
			});
	}

}



