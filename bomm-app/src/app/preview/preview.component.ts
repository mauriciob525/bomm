import { Component, OnInit } from '@angular/core';
import { RegistroService } from '../services/registro.service';
import { Proyecto } from '../formulario/proyecto';
import { Router } from '@angular/router';

@Component({
	selector: 'app-preview',
	templateUrl: './preview.component.html',
	styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

	model:any = {};
	cities = [];
	fill_cities = [];
	department = [];
	contries = [];

	loader:boolean = false;

	generos = []
	tipos = [];
	redes = [];
	niveles_estudio = [];
	cargo_empresa = [];
	tipos_docs = [];
	tipos_video = [];
	tipos_audio = [];
	si_no = [{label:'Si', value:true}, {label:'No', value:false}];
	sexo = [ {id:1, type_gender_name:'Femenino'}, {id:2, type_gender_name:'Masculino'}];

	constructor(private regService: RegistroService, private router:Router) {

		this.model = new Proyecto();

		this.model['audio_video'] = [
			{type_audio_video_id:"", link:""},
			{type_audio_video_id:"", link:""},
			{type_audio_video_id:"", link:""},
			{type_audio_video_id:"", link:""},
			{type_audio_video_id:"", link:""},
			{type_audio_video_id:"", link:""},
		];

		this.model['social'] = [
				{type_social_network_id:"1", link:""},
				{type_social_network_id:"", link:""},
				{type_social_network_id:"", link:""}
			]; 
		this.regService.getInfo().subscribe( data => {
			this.contries = data.country;
			this.department = data.departament;
			this.cities = data.city;
			this.niveles_estudio = data.study_level;
			this.tipos_audio = data.type_audio;
			this.tipos_video = data.type_video;
			this.tipos_docs = data.type_document;
			this.generos = data.type_gender;
			this.sexo = data.type_gender_representative;
			this.tipos = data.type_porposal;
			this.redes = data.type_social_network.filter( red => red.id != 1 );

			this.regService.getProyect().subscribe( data => {

				this.model['audio_video'] = [];
				this.model['social'] = [];



				for(let field in data.project){
					this.model[field] = data.project[field];
				}

				for(let field in data.legal_representative){
					this.model[field] = data.legal_representative[field];
				}

				for(let field of data.audio){
					this.model['audio_video'].push({
						type_audio_video_id: field['type_audio_video_id'],
						link: field['link']
					});
				}

				for(let field of data.video){
					this.model['audio_video'].push({
						type_audio_video_id: field['type_audio_video_id'],
						link: field['link']
					});
				}

				let website = data.social_network.filter( f => f.type_social_network_id == 1)[0];
				
				this.model['social'][0] = website ? {
					type_social_network_id: website['type_social_network_id'],
					link: website['link']
				}: {type_social_network_id:"1", link:""};

				

				for(let field of data.social_network.filter( f => f.type_social_network_id != 1)){
					this.model['social'].push({
						type_social_network_id: field['type_social_network_id'],
						link: field['link']
					});
				}

				if(data.imagen){
					this.model['imagen'] = data.imagen.file;	
				}

				if(data.pdf){
					this.model['pdf'] = data.pdf.file;	
				}
				
				this.model['propuesta_text'] = this.getDataText(this.model['type_proposal_id'], 'tipos', 'type_proposal_name', 'id');
				this.model['genero_text'] = this.getDataText(this.model['type_gender_id'], 'generos', 'type_gender_name', 'id');

				this.model['red1_text'] = this.getDataText(this.model['social'][1].type_social_network_id, 'redes', 'type_social_network_name', 'id');
				this.model['red2_text'] = this.getDataText(this.model['social'][2].type_social_network_id, 'redes', 'type_social_network_name', 'id');			

				this.model['audio1_text'] = this.getDataText(this.model['audio_video'][0].type_audio_video_id, 'tipos_audio', 'type_audio_name', 'id');			
				this.model['audio2_text'] = this.getDataText(this.model['audio_video'][1].type_audio_video_id, 'tipos_audio', 'type_audio_name', 'id');			
				this.model['audio3_text'] = this.getDataText(this.model['audio_video'][2].type_audio_video_id, 'tipos_audio', 'type_audio_name', 'id');			

				this.model['video1_text'] = this.getDataText(this.model['audio_video'][3].type_audio_video_id, 'tipos_video', 'type_audio_name', 'id');			
				this.model['video2_text'] = this.getDataText(this.model['audio_video'][4].type_audio_video_id, 'tipos_video', 'type_audio_name', 'id');			
				this.model['video3_text'] = this.getDataText(this.model['audio_video'][5].type_audio_video_id, 'tipos_video', 'type_audio_name', 'id');			

				this.model['sexo_text'] = this.getDataText(this.model['type_gender_representative_id'], 'sexo', 'type_gender_name', 'id');
				this.model['tdc_text'] = this.getDataText(this.model['type_document_id'], 'tipos_docs', 'type_document_name', 'id');

				this.model['pais_text'] = this.getDataText(this.model['country_id'], 'contries', 'country_name', 'id');
				
				this.model['city_text'] = this.getDataText(this.model['city_id'], 'cities', 'city_name', 'id');

				this.model['nivel_text'] = this.getDataText(this.model['study_level_id'], 'niveles_estudio', 'study_level_name', 'id');
				
				this.model['ccb_text'] = this.getDataText(this.model['ccb'], 'si_no', 'label', 'value');
				this.model['showcases_text'] = this.getDataText(this.model['showcases'], 'si_no', 'label', 'value');

				this.model['represents_company_text'] = this.getDataText(this.model['represents_company'], 'si_no', 'label', 'value');

				this.model['notificationtext_text'] = this.getDataText(this.model['notification_text'], 'si_no', 'label', 'value');
				this.model['notificationemail_text'] = this.getDataText(this.model['notification_email'], 'si_no', 'label', 'value');
			})

		});
	}

	getDataText(input, data, field, id){
		let _data = this[data].filter( opt => opt[id] === input )
		return (_data.length > 0) ? _data[0][field] : "";

	}

	ngOnInit() {
	}

}
