import { Component, OnInit, EventEmitter, Output, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AppSettings } from '../app.settings';

@Component({
	selector: 'app-registro',
	templateUrl: './registro.component.html',
	styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {


	signUpUser:any = {
		name: '',
		email: '',
		emailConfirmation: '',
		password: '',
		passwordConfirmation: ''
	}

	loader:boolean = false;
	errorForm = null;

	@Output() onFormResult = new EventEmitter();
	@ViewChild('f') form;

	constructor(private authService: AuthService, private router:Router) { }

	ngOnInit() { }

	onSignUpSubmit(){
		this.loader = true;
		this.authService.registerUser(this.signUpUser)
			.subscribe(
				res => {
					if(res.status == 200){
						AppSettings.NAME = this.signUpUser.name;
						this.router.navigate(['registro']);
					}
					this.loader = false;
				},
				err => {
					this.errorForm = err.json().errors.full_messages.map( error => {
						let arr_error = error.split(" ");
						arr_error.shift();
						
						console.log(arr_error);
						return arr_error.join(" ");
					}).join('<br/>');
					this.loader = false;

				})
	}

}
