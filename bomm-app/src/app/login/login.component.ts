import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {



	signInUser = {
		email: '',
		password: ''
	};

	errorForm = null;

	@Output() onFormResult = new EventEmitter();

	constructor(private authService: AuthService, private router:Router) { }

	ngOnInit() {
	}

	onSignInSubmit(){
		this.authService.loginInUser(this.signInUser)
			.subscribe( 
				res => {
					if(res.status == 200){
						this.router.navigate(['registro']);
					}
				}, 
				err => {
					this.errorForm = "Credenciales de acceso invalidas. Inténtalo de nuevo.";
				}
			)
	}

	

}
