import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroComponent } from './registro/registro.component';
import { FormularioComponent } from './formulario/formulario.component';
import { LoginComponent } from './login/login.component';
import { RecuperarPasswordComponent } from './recuperar-password/recuperar-password.component';
import { ConfirmarContrasenaComponent } from './confirmar-contrasena/confirmar-contrasena.component';

import { PreviewComponent } from './preview/preview.component';
import { TerminosComponent } from './terminos/terminos.component';
import { GraciasComponent } from './gracias/gracias.component';

import { AuthGuard } from './guards/auth.guard';




const routes: Routes = [
	{
		path: '',
		component: LoginComponent,
    	pathMatch: 'full'
	},
	{
		path: 'crear_cuenta',
		component: RegistroComponent
	},
	{
		path: 'login',
		component: LoginComponent
	},
	{
		path: 'recuperar_contrasena',
		component: RecuperarPasswordComponent
	},
	{
		path: 'confirmar_contrasena/:token',
		component: ConfirmarContrasenaComponent
	},
	{
		path: 'registro',
		component: FormularioComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'confirmar',
		component: PreviewComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'terminos',
		component: TerminosComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'gracias',
		component: GraciasComponent,
		canActivate: [AuthGuard]
	}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
