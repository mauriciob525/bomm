import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmarContrasenaComponent } from './confirmar-contrasena.component';

describe('ConfirmarContrasenaComponent', () => {
  let component: ConfirmarContrasenaComponent;
  let fixture: ComponentFixture<ConfirmarContrasenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmarContrasenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmarContrasenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
