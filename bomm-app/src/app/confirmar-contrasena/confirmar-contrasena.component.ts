import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirmar-contrasena',
  templateUrl: './confirmar-contrasena.component.html',
  styleUrls: ['./confirmar-contrasena.component.scss']
})
export class ConfirmarContrasenaComponent implements OnInit {

  resetPasswordUser = {
		password: '',
		passwordConfirmation: '',
		passwordCurrent: null,
		resetPasswordToken:   '',
	};

	loader:boolean;
	errorForm = null;

	constructor(private authService: AuthService, private router:Router, private route: ActivatedRoute) { }

	ngOnInit() {
		this.route.params.subscribe( params => {
			this.resetPasswordUser.resetPasswordToken = params['token'];
		});
	}

	onSignInSubmit(){
		this.loader = true;
		this.authService.updatePassword(this.resetPasswordUser)
			.subscribe( 
				res => {
					if(res.status == 200){
						this.loader = true;
						this.router.navigate(['registro']);
					}
				}, 
				err => {
					console.log(err);
					this.loader = true;
					this.errorForm = "Hubo un problema al recuperar la contraseña. Inténtalo de nuevo.";
				}
			)
	}

}
