import { Directive, ElementRef, Input } from '@angular/core';

@Directive({ selector: '[imageSize]' })

export class ImagesizeDirective {
	divW:any;
	
	@Input() contenType: string;

	ngAfterContentChecked(){
		var h = this.el.nativeElement.children[0].naturalHeight;
		var w = this.el.nativeElement.children[0].naturalWidth;
		var aspectRatio = w / h;
		if (w > h){
			this.el.nativeElement.children[0].style.height = this.divW + "px";
			this.el.nativeElement.children[0].style.width = (this.divW * aspectRatio) +  "px";
			if( aspectRatio > 1){
				var ml = ((this.divW * aspectRatio) / 2 - (this.divW / 2)) * -1;
				this.el.nativeElement.children[0].style.marginLeft = ml + "px"
			}
		}
		if(aspectRatio < 1){
			var mt = ((this.divW / aspectRatio) / 2 - (this.divW / 2)) * -1;
			this.el.nativeElement.children[0].style.marginTop = mt + "px"
		}
	}
	ngOnInit(){
		this.el.nativeElement.style.height = this.el.nativeElement.clientWidth + "px";
		this.divW = this.el.nativeElement.clientWidth;
	}
    constructor(private el: ElementRef) {
    	
    }
}