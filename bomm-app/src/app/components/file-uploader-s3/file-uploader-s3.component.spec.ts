import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploaderS3Component } from './file-uploader-s3.component';

describe('FileUploaderS3Component', () => {
  let component: FileUploaderS3Component;
  let fixture: ComponentFixture<FileUploaderS3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileUploaderS3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploaderS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
