declare var DZ : any;
import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({
	selector: 'deezer-embed',
	templateUrl: './deezer.component.html',
	styleUrls: ['./deezer.component.scss']
})
export class DeezerComponent implements OnInit, OnChanges {

	@Input() link:string;
	track_id:string;
	failURL:boolean;

	constructor() { }

	ngOnInit() {
		this.playSound(this.link); 	 
	}

	ngOnChanges(changes){
		if(changes.link){
			this.link = changes.link.currentValue;
			this.playSound(this.link);  	
		}
	}

	playSound(url){

		let arr_link = url.split('?')[0].split('/');
		let track_id = arr_link[arr_link.length - 1];

		DZ.init({
			appId  : '1',
			channelUrl : 'http://developers.deezer.com/examples/channel.php',
		});

		DZ.api('track/'+track_id, response => {
			this.failURL = (response.error) ? true : false;
			
			if(!this.failURL){
				this.track_id = "https://www.deezer.com/plugins/player?type=tracks&id="+track_id+"&format=classic&color=007FEB&autoplay=false&playlist=false&width=700&height=240track_id";
			}
			
		});

		
	}	
}
