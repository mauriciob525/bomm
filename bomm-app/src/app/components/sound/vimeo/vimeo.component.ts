import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
  selector: 'vimeo-embed',
  templateUrl: './vimeo.component.html',
  styleUrls: ['./vimeo.component.scss']
})
export class VimeoComponent implements OnInit {

  @Input() link:string;
	html_iframe:string;
	failURL:boolean;

	constructor(private embedService: EmbedVideoService) {
		
	}

	ngOnInit() {
		this.playSound(this.link);
	}

	ngOnChanges(changes){
		if(changes.link){
			this.link = changes.link.currentValue;
			this.playSound(this.link);  	
		}
	}

	playSound(url){
		let arr_sound = url.split('/');
		this.html_iframe = this.embedService.embed_vimeo(arr_sound[arr_sound.length - 1]);
	}

}
