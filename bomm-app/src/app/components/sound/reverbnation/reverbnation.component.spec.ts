import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReverbnationComponent } from './reverbnation.component';

describe('ReverbnationComponent', () => {
  let component: ReverbnationComponent;
  let fixture: ComponentFixture<ReverbnationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReverbnationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReverbnationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
