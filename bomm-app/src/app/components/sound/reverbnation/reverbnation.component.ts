import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({
	selector: 'reverbnation-embed',
	templateUrl: './reverbnation.component.html',
	styleUrls: ['./reverbnation.component.scss']
})
export class ReverbnationComponent implements OnInit, OnChanges {

	@Input() link:string;
	artist:string;
	song_name:string;
	final_link:string;
	failURL:boolean;

	constructor() { }

	ngOnInit() {
		this.playSound(this.link);
	}

	ngOnChanges(changes) {
		if(changes.link){
			this.link = changes.link.currentValue;
			this.playSound(this.link);
		}
	}


	playSound(url){

		if(this.link.indexOf('reverbnation') > 0){
			let arr_link = this.link.split('/');

			if(arr_link.length == 6 && arr_link[5].length > 0){
				this.artist = arr_link[3];
				this.song_name = arr_link[5].split('-')[0];

				this.final_link = 'https://www.reverbnation.com/widget_code/html_widget/'+this.artist+'?widget_id=55&pwc[song_ids]='+this.song_name+'&context_type=song&pwc[size]=small';	
				this.failURL = false;
			}else{
				this.failURL = true;
			}
		}else{
			this.failURL = true;
		}
		
		
		
	}

}
