import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';
import * as bandcamp from 'bandcamp-scraper';


@Component({
	selector: 'bandcamp-embed',
	templateUrl: './bandcamp.component.html',
	styleUrls: ['./bandcamp.component.scss']
})
export class BandcampComponent implements OnInit {

	@Input() link:string;
	track_id:string;
	failURL:boolean;
	url_link:string = environment.token_auth_config.apiBase + "/guia_audios.pdf";

	constructor() { }

	ngOnInit() {
		
		this.playSound(this.link); 	 
	}

	ngOnChanges(changes){
		if(changes.link){
			this.link = changes.link.currentValue;
			this.playSound(this.link);  	
		}
	}

	playSound(url){

		try{
			bandcamp.getAlbumInfo(url, (error, response) => {
				if(error){
					this.failURL = true;
				}else{
					this.failURL = false;
					this.track_id = "https://bandcamp.com/EmbeddedPlayer/album="+response.raw.current.album_id+"/size=small/bgcol=ffffff/linkcol=0687f5/track="+response.raw.id+"/transparent=true/"
				}
			})	
		}catch(err){
			this.failURL = false;
			
		}
		
	}

}
