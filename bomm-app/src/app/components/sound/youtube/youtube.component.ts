import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';

@Component({
	selector: 'youtube-embed',
	templateUrl: './youtube.component.html',
	styleUrls: ['./youtube.component.scss']
})
export class YoutubeComponent implements OnInit, OnChanges {


	@Input() link:string;
	html_iframe:string;
	failURL:boolean;

	constructor(private embedService: EmbedVideoService) {
		
	}

	ngOnInit() {
		this.playSound(this.link);
	}

	ngOnChanges(changes){
		if(changes.link){
			this.link = changes.link.currentValue;
			this.playSound(this.link);  	
		}
	}

	playSound(url){
		let arr_sound = url.split('v=');
		this.html_iframe = this.embedService.embed_youtube(arr_sound[arr_sound.length - 1]);
	}

}
