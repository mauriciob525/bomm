import { Component, OnInit, OnChanges, Input } from '@angular/core';
import * as SC from "soundcloud"

@Component({
  selector: 'soundcloud-embed',
  templateUrl: './soundcloud.component.html',
  styleUrls: ['./soundcloud.component.scss']
})
export class SoundcloudComponent implements OnInit, OnChanges {

	@Input() link:string;
	html_iframe:string;
	failURL:boolean;

  constructor() { }

  ngOnInit() {
	  this.playSound(this.link);  	
  }

  ngOnChanges(changes){
  	if(changes.link){
  		this.link = changes.link.currentValue;
  		this.playSound(this.link);  	
  	}
  }

  playSound(url){
  	let self = this;

  	SC.oEmbed(url, {
  		maxheight: 160,
  		show_comments:false,

  	}).then(function(embed){
  		self.html_iframe = embed.html;
  		self.failURL = false;
	}).catch( err => self.failURL = true);
  }
}
