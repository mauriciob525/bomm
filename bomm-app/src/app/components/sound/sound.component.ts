import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { SpotifyComponent } from './spotify/spotify.component';

@Component({
	selector: 'sound-embed',
	templateUrl: './sound.component.html',
	styleUrls: ['./sound.component.scss']
})
export class SoundComponent implements OnInit, OnChanges {


	@Input() source_sound:string;
	@Input() type_sound: number;
	@Input() error_same: boolean;

	source_link:string;

	constructor() { }

	ngOnInit() {
		this.source_link = this.source_sound;
	}

	ngOnChanges(changes){
		if(changes.source_sound){
			this.source_link = changes.source_sound.currentValue
		}
		
	}
}
