import { Component, OnInit, OnChanges, Input  } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

@Component({
	selector: 'mixcloud-embed',
	templateUrl: './mixcloud.component.html',
	styleUrls: ['./mixcloud.component.scss']
})
export class MixcloudComponent implements OnInit  {

	@Input() link:string;
	html_iframe:string;
	failURL:boolean;

	constructor(private http: Http) { }


	ngOnInit() {
		this.playSound(this.link); 	 
	}

	ngOnChanges(changes){
		if(changes.link){
			this.link = changes.link.currentValue;
			this.playSound(this.link);  	
		}
	}

	playSound(url){

		let uri_encoded = encodeURIComponent(url);

		let requestOptions = new RequestOptions({ headers:null, withCredentials: true });
		this.http.get("https://www.mixcloud.com/oembed/?url="+uri_encoded+"&format=json", requestOptions)
			.map( (response:Response) => {
				return response.json()
			})
			.subscribe( 
				embed => {
					this.failURL = false;
					this.html_iframe = embed.html;
				},
				error => this.failURL = true
			);

	}

}
