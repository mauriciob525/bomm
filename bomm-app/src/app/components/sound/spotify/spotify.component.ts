import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

@Component({
	selector: 'spotify-embed',
	templateUrl: './spotify.component.html',
	styleUrls: ['./spotify.component.scss']
})
export class SpotifyComponent implements OnInit, OnChanges {


	@Input() link:string = "";
	failURL:boolean;

	constructor(private http: Http) { }

	ngOnInit() {
		this.playSound(this.link);  	
	}

	ngOnChanges(changes){
		if(changes.link){
			this.link = changes.link.currentValue;
			this.playSound(this.link);  	
		}
	}

	playSound(url){
		console.log(url.length);
		if(url.length == 0){
			this.failURL = true;
		}else{
			if(url.indexOf('open.spotify.com') > 0){
				// this.http.get(url)
				// .map( (response:Response) => response.statusText)
				// .subscribe( 
				// 	data => {
				// 		if(Number(data) == 200)
				// 			this.failURL = false;
				// 	},
				// 	error => this.failURL = true
				// );
				this.failURL = false;
			}else{
				this.failURL = true
			}
		}
	}

}
