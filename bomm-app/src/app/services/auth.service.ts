import { Injectable } from '@angular/core';
import { Angular2TokenService } from 'angular2-token';
import { Subject, Observable } from 'rxjs';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';



@Injectable()
export class AuthService {

	userSignedIn$: Subject<boolean> = new Subject();

	constructor(private authService:Angular2TokenService) { 
		this.authService.validateToken()
		.subscribe(res => res.status == 200 ? 
			this.userSignedIn$.next(res.json().success) : 
			this.userSignedIn$.next(false));
	}

	logOutUser():Observable<Response>{
		return this.authService.signOut()
		.map(res => {
			this.userSignedIn$.next(false);
			return res;
		})
	}

	registerUser(singUpData: {name:string, email:string, password:string, passwordConfirmation:string}):Observable<Response>{
		return this.authService.registerAccount(singUpData)
		.map(res => {
			this.userSignedIn$.next(true);
			return res;
		});
	}

	loginInUser(singInData: {email:string, password:string}):Observable<Response>{
		return this.authService.signIn(singInData)
			.map( res => {
				this.userSignedIn$.next(true);
				return res;
			})
	}

	forgotPassword(resetPasswordData: {email:string}):Observable<Response>{
		return this.authService.resetPassword(resetPasswordData)
			.map( res => {
				return res;
			})
	}

	updatePassword(resetPasswordData: { password:string, passwordConfirmation:string, passwordCurrent:string, resetPasswordToken:string}):Observable<Response>{
		return this.authService.post('/index/reset_password', resetPasswordData)
			.map( res => {
				return res;
			})
	}

}
