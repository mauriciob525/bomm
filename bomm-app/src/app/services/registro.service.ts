import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { AppSettings } from '../app.settings';
import  {Angular2TokenService} from 'angular2-token';

import { Http, Headers, Response, URLSearchParams } from '@angular/http';

@Injectable()
export class RegistroService {

  constructor(private http: Http, private tokenService:Angular2TokenService) {
  }
  
  getInfo(){
  	return  this.tokenService.get('getInfo', null)
    .map((response: Response) => response.json());
  }

  createProyect(modelData){
  	modelData.uid = this.tokenService.currentAuthData.uid;

  	return this.tokenService.post('create_project', modelData)
    .map((response: Response) => response.json());
  }

  getProyect(){
    return this.tokenService.post('getProject', {uid:this.tokenService.currentAuthData.uid})
    .map((response: Response) => response.json()); 

  }

}
